// 查询列表接口
function getDishPage (params) {
  return $axios({
    url: `api/dishs/nosort/${params.currentPage}/${params.pageSize}`,
    method: 'get',
    data: { ...params }
  })
}

function getDishPage2 (params) {  // +模糊
  return $axios({
    url: `api/dishs/nosort/${params.currentPage}/${params.pageSize}/?name=${params.name}`,
    method: 'get',
    data: { ...params }
  })
}
// 删除接口
function deleteDish (id) {
  return $axios({
    url: `api/dishs/${id}`,
    method: 'delete',
    data: {id }
  })
}
/*const deleteDish = (id) => {
  return $axios({
    url: 'api/dishs',
    method: 'delete',
    params: { id }
  })
}*/



// 修改接口
const editDish = (params) => {
  return $axios({
    url: 'api/dishs/',
    method: 'put',
    data: { ...params }
  })
}

function addDish (params) {
  return $axios({
    url: 'api/dishs',
    method: 'post',
    data: { ...params }
  })
}

// 新增接口
/*const addDish = (params) => {
  return $axios({
    url: 'api/dishs',
    method: 'post',
    data: { ...params }
  })
}*/

// 查询详情
const queryDishById = (id) => {
  return $axios({
    url: `api/dishs/${id}`,
    method: 'get'
  })
}

// 获取菜品分类列表
const getCategoryList = (params) => {
  return $axios({
    url: 'api/categorys/list',
    method: 'get',
    params
  })
}

// 查菜品列表的接口
const queryDishList = (params) => {
  return $axios({
    url: 'api/dishs',
    method: 'get',
    params
  })
}

// 文件down预览
const commonDownload = (params) => {
  return $axios({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    url: 'api/common/download/?name=${params.imageUrl}',
    method: 'get',
    params
  })
}

// 起售停售---起售停售接口
function dishStatusByStatus (params) {
  return $axios({
    url: `api/dishs/enable/${params.id}`,
    method: 'put',
    params: { ...params }
  })
}
function dishStatusByStatus2 (params) {
  return $axios({
    url: `api/dishs/stop/${params.id}`,
    method: 'put',
    params: { ...params }
  })
}
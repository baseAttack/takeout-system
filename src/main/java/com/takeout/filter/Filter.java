package com.takeout.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @date 2022/5/29-6:38
 */
//@WebFilter(filterName = "filterEmp", urlPatterns = "/*")
public class Filter implements javax.servlet.Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 过滤器初始化
        javax.servlet.Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // 过滤器执行
        if (servletRequest instanceof HttpServletRequest) {
            // do sth
            //获取请求的URI地址
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            String requestURI = req.getRequestURI();
            if (requestURI.endsWith("login.html") ||
                    requestURI.endsWith("index.html") ||
                    requestURI.endsWith(".js") ||
                    requestURI.endsWith(".css") ||
                    requestURI.contains("images") ||
                    requestURI.contains("test") ||
                    requestURI.contains("/employees/login") ||
                    requestURI.contains("/users")
            ) { // 除了这些放行，
                // next filter
                filterChain.doFilter(servletRequest, servletResponse);
            } else {    // 其他全部拦截
                //session
                HttpSession session = req.getSession();
                Object employee = session.getAttribute("employee");
                Object user = session.getAttribute("user");
                if (employee != null || user != null) {
                    //放行
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    //判断请求是否为Ajax请求
                    if (req.getHeader("x-requested-with") != null && req.getHeader("x-requested-with").equals("XMLHttpRequest")) {
                        resp.getWriter().write("sessionTimeOut");
                    } else {
                        //重定向到登录页面
                        resp.sendRedirect(req.getContextPath() + "/static/login.html");
                    }
                }

            }
            // do sth


            //
        }


    }

    @Override
    public void destroy() {
        // 过滤器销毁
        javax.servlet.Filter.super.destroy();
    }
}

package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("address_book")
@ApiModel(value = "地址", description = "地址实体类")
public class AddressBook implements Serializable {
    // 主键
    private Long id;
    //用户id
    private Long userId;
    //收货人
    private String consignee;
    //手机号
    private String phone;
    //性别0女  1男
    private Integer sex;
    //省级区划编号
    private String provinceCode;
    //省级名称
    private String provinceName;
    //城市编号
    private String cityCode;
    //城市名称
    private String cityName;
    //区级编号
    private String districtCode;
    //区级名称
    private String districtName;
    //详细地址
    private String detail;
    //标签
    private String label;
    //是否默认  0否 1是
    private Integer isDefault;
    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    //更新时间
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;
    //创建用户
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;
    //更新用户信息
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
    //是否删除,1:已经删除，0:没有删除
    private Integer isDeleted;
}

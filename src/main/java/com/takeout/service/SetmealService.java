package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.Setmeal;

/**
 * @date 2022/5/22-19:33
 */
public interface SetmealService extends IService<Setmeal> {
    IPage<Setmeal> getPage(int currentPage, int pageSize);
    IPage<Setmeal> getPage(int currentPage, int pageSize, Setmeal setmeal);
}

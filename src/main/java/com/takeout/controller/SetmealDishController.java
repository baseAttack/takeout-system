package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.Dish;
import com.takeout.entity.SetmealDish;
import com.takeout.service.SetmealDishService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @date 2022/5/22-20:19
 */
@RestController
@RequestMapping("/setmealdishs")
@Slf4j
@Api(tags = "套餐和菜品关系管理接口")
public class SetmealDishController {
    @Autowired
    private SetmealDishService setmealDishService;

    // 1、CRUD
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<SetmealDish> list = setmealDishService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody SetmealDish setmealDish) {
//        // 菜品名不能一样
//        LambdaQueryWrapper<SetmealDish> qw = new LambdaQueryWrapper<>();
//        qw.eq(SetmealDish::getName, setmealDish.getName());
//        if (setmealDishService.count(qw) != 0) {
//            return Result.fail("菜品名已存在");
//        }

        // 公共字段: 哪个用户添加了此套餐
        Long id = (Long)request.getSession().getAttribute("employee");
        setmealDish.setCreateUser(id);
        setmealDish.setUpdateUser(id);
        // 保存
        boolean flag = setmealDishService.save(setmealDish);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody SetmealDish setmealDish) {
        // 菜品名不能一样
//        LambdaQueryWrapper<SetmealDish> qw = new LambdaQueryWrapper<>();
//        qw.eq(SetmealDish::getName, setmealDish.getName());
//        if (setmealDishService.count(qw) != 0) {
//            return Result.fail("菜品名已存在");
//        }

        // 公共字段: 哪个用户添加了此套餐
        Long id = (Long)request.getSession().getAttribute("employee");
        setmealDish.setUpdateUser(id);
        // 更新
        boolean flag = setmealDishService.updateById(setmealDish);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = setmealDishService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        SetmealDish setmealDish = setmealDishService.getById(id);
        if (setmealDish == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", setmealDish);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          SetmealDish setmealDish) {
        IPage<SetmealDish> page = setmealDishService.getPage(currentPage, pageSize, setmealDish);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    // 2、业务

}

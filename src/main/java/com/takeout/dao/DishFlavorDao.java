package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-14:52
 */
@Mapper
public interface DishFlavorDao extends BaseMapper<DishFlavor> {
}

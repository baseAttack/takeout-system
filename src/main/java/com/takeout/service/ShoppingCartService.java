package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
    IPage<ShoppingCart> getPage(int currentPage, int pageSize);
    IPage<ShoppingCart> getPage(int currentPage, int pageSize, ShoppingCart shoppingCart);
}

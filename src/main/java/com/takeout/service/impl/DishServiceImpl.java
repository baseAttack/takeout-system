package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.DishDao;
import com.takeout.dao.DishFlavorDao;
import com.takeout.entity.Dish;
import com.takeout.entity.Employee;
import com.takeout.service.DishService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-14:53
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishDao, Dish> implements DishService {
    @Autowired
    private DishDao dishDao;


    @Override
    public IPage<Dish> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        dishDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Dish> getPage(int currentPage, int pageSize, Dish dish) {
        LambdaQueryWrapper<Dish> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(dish.getName()),
                Dish::getName,
                dish.getName());
        lqw.like(Strings.isNotEmpty(dish.getName()),
                Dish::getName,
                dish.getName());

        IPage page = new Page(currentPage, pageSize);
        dishDao.selectPage(page, lqw);
        return page;
    }

    @Override
    public IPage<Dish> getPageBySort(int currentPage, int pageSize, Dish dish) {
        LambdaQueryWrapper<Dish> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(dish.getName()),
                Dish::getName,
                dish.getName());
        // category_id
        lqw.like(dish.getCategoryId() != null,
                Dish::getCategoryId,
                dish.getCategoryId());
        lqw.like(Strings.isNotEmpty(dish.getName()),
                Dish::getName,
                dish.getName());
        // 按照sort排序
        lqw.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);

        IPage page = new Page(currentPage, pageSize);
        dishDao.selectPage(page, lqw);
        return page;
    }
}

package com.takeout.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.OrderDetail;
import com.takeout.service.OrderDetailService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @date 2022/5/22-20:17
 */
@Slf4j
@RestController
@RequestMapping("/orderdetails")
@Api(tags = "订单详情管理接口")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderDetailService;

    // 1. CRUD
    // 1、CRUD开发, 可省略
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<OrderDetail> list = orderDetailService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody OrderDetail orderDetail) {
        // 保存
        boolean flag = orderDetailService.save(orderDetail);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody OrderDetail orderDetail) {
        // 更新
        boolean flag = orderDetailService.updateById(orderDetail);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = orderDetailService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
            OrderDetail orderDetail = orderDetailService.getById(id);
        if (orderDetail == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", orderDetail);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          OrderDetail orderDetail) {
        IPage<OrderDetail> page = orderDetailService.getPage(currentPage, pageSize, orderDetail);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    // 2. 业务


}

package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.MyUser;
import com.takeout.entity.Orders;
import com.takeout.entity.ShoppingCart;
import com.takeout.service.OrdersService;
import com.takeout.service.ShoppingCartService;
import com.takeout.service.UserService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/shoppingCarts")
@Api(tags = "购物车管理接口")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrdersService ordersService;

    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<ShoppingCart> list = shoppingCartService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody ShoppingCart shoppingCart) {
        shoppingCart.setCreateTime(LocalDateTime.now());
        boolean flag = false;
        // 检查购物车中有没有该用户的该菜品/套餐
        if (Objects.equals(shoppingCart.getName(), "dish")) {// 1 菜品
            LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
            qw.eq(ShoppingCart::getUserId, shoppingCart.getUserId());
            qw.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
            ShoppingCart one = shoppingCartService.getOne(qw);
            if (one != null) {// 有, 数量+
                one.setNumber(one.getNumber()+shoppingCart.getNumber());
                one.setAmount(one.getAmount().add(shoppingCart.getAmount()));
                flag = shoppingCartService.updateById(one);
            } else { // 没有
                flag = shoppingCartService.save(shoppingCart);
            }
        } else if(Objects.equals(shoppingCart.getName(), "setmeal")) {// 2 套餐
            LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
            qw.eq(ShoppingCart::getUserId, shoppingCart.getUserId());
            qw.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
            ShoppingCart one = shoppingCartService.getOne(qw);
            if (one != null) {// 有, 数量+,价格+
                one.setNumber(one.getNumber()+shoppingCart.getNumber());
                one.setAmount(one.getAmount().add(shoppingCart.getAmount()));
                flag = shoppingCartService.updateById(one);
            } else { // 没有
                flag = shoppingCartService.save(shoppingCart);
            }
        }
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody ShoppingCart shoppingCart) {
        // 更新
        boolean flag =shoppingCartService.updateById(shoppingCart);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = shoppingCartService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        ShoppingCart shoppingCart = shoppingCartService.getById(id);
        if (shoppingCart == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", shoppingCart);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          ShoppingCart shoppingCart) {
        IPage<ShoppingCart> page = shoppingCartService.getPage(currentPage, pageSize, shoppingCart);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    // 2、具体业务开发
    @ApiOperation(value = "通过用户id查购物车里所有的商品")
    @GetMapping("/getcarts/{id}")
    public Result getCarts(@PathVariable int id) {
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        qw.eq(ShoppingCart::getUserId, id);
        List<ShoppingCart> list = shoppingCartService.list(qw);
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "用户结账")
    @GetMapping("/pay/{id}")
    public Result pay(@PathVariable int id) {
        // 从购物车中拿到数据
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        qw.eq(ShoppingCart::getUserId, id);
        List<ShoppingCart> list = shoppingCartService.list(qw);


        // 计算总价格
        BigDecimal sum = BigDecimal.valueOf(0);
        for (ShoppingCart shoppingCart : list) {
            sum = sum.add(shoppingCart.getAmount());
        }

        // 写入订单表
        MyUser user = userService.getById(id);

        Orders orders = new Orders(
                null,
                UUID.randomUUID().toString(),
                1,
                (long) id,
                0L,
                LocalDateTime.now(),
                LocalDateTime.now(),
                1,
                Double.parseDouble(String.valueOf(sum)),
                "无备注",
                user.getPhone(),
                "某地址",
                user.getName(),
                user.getPhone().toString()
        );

        ordersService.save(orders);

        // 删除购物车的数据
        LambdaQueryWrapper<ShoppingCart> qw2 = new LambdaQueryWrapper<>();
        qw2.eq(ShoppingCart::getUserId, id);
        shoppingCartService.remove(qw2);

        return Result.success("支付成功,订单编号"+orders.getNumber());
    }

}

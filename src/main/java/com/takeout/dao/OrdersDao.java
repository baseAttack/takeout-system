package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-19:52
 */
@Mapper
public interface OrdersDao extends BaseMapper<Orders> {
}

function loginApi(data) {
  return $axios({
    'url': 'api/employees/login',
    'method': 'post',
    data
  })
}

function logoutApi(){
  return $axios({
    'url': 'api/employees/layout',
    'method': 'post',
  })
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.Orders;

/**
 * @date 2022/5/22-19:53
 */
public interface OrdersService extends IService<Orders> {
    IPage<Orders> getPage(int currentPage, int pageSize);
    IPage<Orders> getPage(int currentPage, int pageSize, Orders orders);
}

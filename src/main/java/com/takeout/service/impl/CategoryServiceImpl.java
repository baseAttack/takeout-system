package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.CategoryDao;
import com.takeout.entity.Category;
import com.takeout.entity.Dish;
import com.takeout.entity.Setmeal;
import com.takeout.service.CategoryService;
import com.takeout.service.DishService;
import com.takeout.service.SetmealService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-19:21
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, Category> implements CategoryService {
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private DishService dishService;
    @Autowired
    private SetmealService setmealService;


    @Override
    public boolean remove(long id) {
        LambdaQueryWrapper<Dish> qw = new LambdaQueryWrapper<>();
        // 检查菜品表中是否有该分类下的菜品
        qw.eq(Dish::getCategoryId,id);
        if (dishService.count(qw) != 0){    // 有
            return false;
        }

        // 检查套餐表中是否有该分类下的套餐
        LambdaQueryWrapper<Setmeal> qw2 = new LambdaQueryWrapper<>();
        qw2.eq(Setmeal::getCategoryId,id);
        if (setmealService.count(qw2) != 0){    // 有
            return false;
        }
        // 哈哈,是个空的分类，立即删除!
        super.removeById(id);
        return true;
    }

    @Override
    public IPage<Category> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        categoryDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Category> getPage(int currentPage, int pageSize, Category category) {
        LambdaQueryWrapper<Category> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(null != category.getType(),
                Category::getType,
                category.getType());
        lqw.like(Strings.isNotEmpty(category.getName()),
                Category::getName,
                category.getName());

        IPage page = new Page(currentPage, pageSize);
        categoryDao.selectPage(page, lqw);
        return page;
    }

    @Override
    public IPage<Category> getPageSplitByType(int currentPage, int pageSize, Category category) {
        // 条件构造器
        LambdaQueryWrapper<Category> qw = new LambdaQueryWrapper<>();
        // 添加条件
        qw.eq(category.getType() != null,
                Category::getType,
                category.getType());

        // 模糊查询条件
        qw.like(null != category.getType(),
                Category::getType,
                category.getType());
        qw.like(Strings.isNotEmpty(category.getName()),
                Category::getName,
                category.getName());

        // 排序条件添加: sort小在前，数据的最新在前
        qw.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        IPage page = new Page(currentPage, pageSize);
        categoryDao.selectPage(page, qw);
        return page;
    }
}

package com.takeout.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.AddressBookDao;
import com.takeout.entity.AddressBook;
import com.takeout.service.AddressBookService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookDao, AddressBook> implements AddressBookService {
    @Autowired
    private AddressBookDao addressBookDao;

    @Override
    public IPage<AddressBook> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        addressBookDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<AddressBook> getPage(int currentPage, int pageSize, AddressBook addressBook) {
        LambdaQueryWrapper<AddressBook> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(addressBook.getConsignee()),
                AddressBook::getConsignee,
                addressBook.getConsignee());

        IPage page = new Page(currentPage, pageSize);
        addressBookDao.selectPage(page, lqw);
        return page;
    }
}

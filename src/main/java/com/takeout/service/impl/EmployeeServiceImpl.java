package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.EmployeeDao;
import com.takeout.entity.Employee;
import com.takeout.entity.EmployeeInfo;
import com.takeout.service.EmployeeService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-10:45
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeDao, Employee> implements EmployeeService {
    @Autowired
    private EmployeeDao employeeDao;

    @Override
    public IPage<Employee> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        employeeDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Employee> getPage(int currentPage, int pageSize, Employee employee) {
        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(employee.getName()),    // 姓名查询--等
                Employee::getName,
                employee.getName());
        lqw.like(Strings.isNotEmpty(employee.getUsername()),
                Employee::getUsername,
                employee.getUsername());
        lqw.like(Strings.isNotEmpty(employee.getPhone()),
                Employee::getPhone,
                employee.getPhone());
        lqw.like(Strings.isNotEmpty(employee.getSex()),
                Employee::getSex,
                employee.getSex());
        lqw.like(Strings.isNotEmpty(employee.getIdNumber()),
                Employee::getIdNumber,
                employee.getIdNumber());
        lqw.like(employee.getStatus() != null,
                Employee::getStatus,
                employee.getStatus());

        IPage page = new Page(currentPage, pageSize);
        employeeDao.selectPage(page, lqw);
        return page;
    }

    @Override
    public IPage<Employee> getPage(int currentPage, int pageSize, EmployeeInfo employeeInfo) {

        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(employeeInfo.getName()),    // 姓名查询--等
                Employee::getName,
                employeeInfo.getName());
        lqw.like(Strings.isNotEmpty(employeeInfo.getUsername()),
                Employee::getUsername,
                employeeInfo.getUsername());
        lqw.like(Strings.isNotEmpty(employeeInfo.getPhone()),
                Employee::getPhone,
                employeeInfo.getPhone());
        lqw.like(Strings.isNotEmpty(employeeInfo.getSex()),
                Employee::getSex,
                employeeInfo.getSex());
        lqw.like(Strings.isNotEmpty(employeeInfo.getIdNumber()),
                Employee::getIdNumber,
                employeeInfo.getIdNumber());
        lqw.like(employeeInfo.getStatus() != null,
                Employee::getStatus,
                employeeInfo.getStatus());

        IPage page = new Page(currentPage, pageSize);

        employeeDao.selectPage(page, lqw);
        return page;
    }
}

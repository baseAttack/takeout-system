package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2022/5/22-14:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("dish_flavor")
public class DishFlavor {
    private Long id;
    private Long dishId;
    private String name;
    private String value;
    private Integer isDeleted;
    // 公共字段自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT)    // 有点问题，那就手动添加吧
    private Long createUser;
    @TableField(fill = FieldFill.INSERT_UPDATE) // 有点问题，那就手动添加吧
    private Long updateUser;
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.Dish;

/**
 * @date 2022/5/22-14:53
 */
public interface DishService extends IService<Dish> {
    IPage<Dish> getPage(int currentPage, int pageSize);
    IPage<Dish> getPage(int currentPage, int pageSize, Dish dish);
    IPage<Dish> getPageBySort(int currentPage, int pageSize, Dish dish);
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.AddressBook;
import com.takeout.entity.Employee;

public interface AddressBookService extends IService<AddressBook> {
    IPage<AddressBook> getPage(int currentPage, int pageSize);
    IPage<AddressBook> getPage(int currentPage, int pageSize, AddressBook addressBook);
}


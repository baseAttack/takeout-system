package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.takeout.entity.Orders;
import com.takeout.entity.Setmeal;
import com.takeout.service.OrdersService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @date 2022/5/22-20:18
 */
@Slf4j
@RestController
@RequestMapping("/orders")
@Api(tags = "订单管理接口")
public class OrdersContoller {
    // 后台管理 最后一步

    @Autowired
    private OrdersService ordersService;

    // 1、CRUD开发, 可省略
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<Orders> list = ordersService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody Orders orders) {
        orders.setNumber(UUID.randomUUID().toString());

        // 保存
        boolean flag = ordersService.save(orders);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody Orders orders) {
        LambdaQueryWrapper<Orders> qw = new LambdaQueryWrapper<>();
        qw.eq(Orders::getNumber, orders.getNumber());
        if (ordersService.count(qw) != 0) {
            return Result.fail("订单编号已存在");
        }

        // 更新
        boolean flag = ordersService.updateById(orders);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = ordersService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        Orders orders = ordersService.getById(id);
        if (orders == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", orders);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          Orders orders) {
        IPage<Orders> page = ordersService.getPage(currentPage, pageSize, orders);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

//    // 2 实际业务开发
//    // 时间字段比较--模糊查询
//    @ApiOperation(value = "时间字段比较--模糊查询")
//    @GetMapping("/time")
//    public Result getPage(String startTime, String endTime) {
//        LocalDateTime startTime2 = LocalDateTime.of(2022,5,25,2,2,2);
//        LocalDateTime endTime2 = LocalDateTime.of(2022,5,26,12,2,2);
//        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
//        queryWrapper.between(Orders::getOrderTime, startTime, endTime);
//        List<Orders> list = ordersService.list(queryWrapper);
//
//
//        if (list == null) {
//            return Result.fail("查询失败");
//        } else {
//            return Result.success("查询成功", list);
//        }
//    }
}

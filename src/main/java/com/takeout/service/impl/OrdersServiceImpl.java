package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.OrdersDao;
import com.takeout.entity.Dish;
import com.takeout.entity.Orders;
import com.takeout.service.OrdersService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-19:54
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersDao, Orders> implements OrdersService {
    @Autowired
    private OrdersDao ordersDao;

    @Override
    public IPage<Orders> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        ordersDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Orders> getPage(int currentPage, int pageSize, Orders orders) {
        LambdaQueryWrapper<Orders> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(orders.getNumber()),
                Orders::getNumber,
                orders.getNumber());

        IPage page = new Page(currentPage, pageSize);
        ordersDao.selectPage(page, lqw);
        return page;
    }
}

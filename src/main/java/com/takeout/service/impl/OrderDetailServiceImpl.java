package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.OrderDetailDao;
import com.takeout.entity.Dish;
import com.takeout.entity.OrderDetail;
import com.takeout.service.OrderDetailService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-20:08
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailDao, OrderDetail> implements OrderDetailService {
    @Autowired
    private OrderDetailDao orderDetailDao;

    @Override
    public IPage<OrderDetail> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        orderDetailDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<OrderDetail> getPage(int currentPage, int pageSize, OrderDetail orderDetail) {
        LambdaQueryWrapper<OrderDetail> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(orderDetail.getName()),
                OrderDetail::getName,
                orderDetail.getName());

        IPage page = new Page(currentPage, pageSize);
        orderDetailDao.selectPage(page, lqw);
        return page;
    }
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.DishFlavor;

/**
 * @date 2022/5/22-14:53
 */
public interface DishFlavorService extends IService<DishFlavor> {
    IPage<DishFlavor> getPage(int currentPage, int pageSize);
    IPage<DishFlavor> getPage(int currentPage, int pageSize, DishFlavor dishFlavor);
}

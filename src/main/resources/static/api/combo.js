// 查询列表数据
function getSetmealPage(params){
  return $axios({
    url: `api/setmeals/${params.currentPage}/${params.pageSize}`,
    method: 'get',
    data: { ...params }
  })
}

// 删除数据接口
const deleteSetmeal = (id) => {
  return $axios({
    url: `api/setmeals/${id}`,
    method: 'delete',
    params: { id }
  })
}

// 修改数据接口
const editSetmeal = (params) => {
  return $axios({
    url: '/api/setmeals',
    method: 'put',
    data: { ...params }
  })
}

// 新增数据接口
const addSetmeal = (params) => {
  return $axios({
    url: '/api/setmeals',
    method: 'post',
    data: { ...params }
  })
}

// 查询详情接口
const querySetmealById = (id) => {
  return $axios({
    url: `api/setmeals/${id}`,
    method: 'get'
  })
}

//停售
function setmealStatusByStatus (params) {
  return $axios({
    url: `api/setmeals/stop/${params.id}`,
    method: 'put',
    params: { ...params }
  })
}
//启售
function setmealStatusByStatus2 (params) {
  return $axios({
    url: `api/setmeals/enable/${params.id}`,
    method: 'put',
    params: { ...params }
  })
}


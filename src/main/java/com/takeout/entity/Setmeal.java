package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2022/5/22-14:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("setmeal")
public class Setmeal {
    private Long id;
    private Long categoryId;
    private String name;
    private double price;
    private Integer status;
    private String code;
    private String description;
    private String image;
    // 公共字段自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT)    // 手动添加吧
    private Long createUser;
    @TableField(fill = FieldFill.INSERT_UPDATE) // 手动添加吧
    private Long updateUser;
}

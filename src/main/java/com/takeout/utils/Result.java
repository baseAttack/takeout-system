package com.takeout.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date 2022/5/22-10:15
 * 前后端交互消息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Boolean flag;   // 成功/失败
    private String msg;     // 返回消息
    private Object data;    // 保存数据

    /**
     * 返回成功与否
     * @param flag
     */
    public Result(Boolean flag) {
        this.flag = flag;
    }

    /**
     * 标志+数据
     * @param flag
     * @param obj
     */
    public Result(Boolean flag, Object obj) {
        this.flag = flag;
        this.data = obj;
    }


    /**
     * 失败消息
     * @param msg
     * @return
     */
    public static Result fail(String msg) {
        return new Result(false, msg, null);
    }

    /**
     * 成功消息
     * @param msg
     * @return
     */
    public static Result success(String msg) {
        return new Result(true, msg, null);
    }

    /**
     * 成功消息 2
     * @param msg
     * @param obj
     * @return
     */
    public static Result success(String msg, Object obj) {
        return new Result(true, msg, obj);
    }
}

package com.takeout.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.MyUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/24-19:52
 */
@Mapper
public interface UserDao extends BaseMapper<MyUser> {

}

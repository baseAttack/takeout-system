package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.Employee;
import com.takeout.entity.EmployeeInfo;

/**
 * @date 2022/5/22-10:44
 */
public interface EmployeeService extends IService<Employee> {
    IPage<Employee> getPage(int currentPage, int pageSize);
    IPage<Employee> getPage(int currentPage, int pageSize, Employee employee);
    IPage<Employee> getPage(int currentPage, int pageSize, EmployeeInfo employeeInfo);
}

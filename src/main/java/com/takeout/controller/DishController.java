package com.takeout.controller;

import ch.qos.logback.core.pattern.util.RegularEscapeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.Dish;
import com.takeout.entity.SetmealDish;
import com.takeout.service.DishService;
import com.takeout.service.SetmealDishService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @date 2022/5/22-15:02
 */
@Slf4j
@RestController
@RequestMapping("/dishs")
@Api(tags = "菜品管理接口")
public class DishController {
    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealDishService setmealDishService;

    // 1、CRUD开发, 可省略
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<Dish> list = dishService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody Dish dish) {
//        // 菜品名不能一样
//        LambdaQueryWrapper<Dish> qw = new LambdaQueryWrapper<>();
//        qw.eq(Dish::getName, dish.getName());
//        if (dishService.count(qw) != 0) {
//            return Result.fail("菜品名已存在");
//        }

        dish.setCodevarchar(UUID.randomUUID().toString());

        // 公共字段
        Long id = (Long) request.getSession().getAttribute("employee");
        dish.setCreateUser(id);
        dish.setUpdateUser(id);

        // 保存
        boolean flag = dishService.save(dish);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody Dish dish) {
        // 菜品名不能一样
//        LambdaQueryWrapper<Dish> qw = new LambdaQueryWrapper<>();
//        qw.eq(Dish::getName, dish.getName());
//        if (dishService.count(qw) != 0) {
//            return Result.fail("菜品名已存在");
//        }

        // 公共字段
        Long id = (Long) request.getSession().getAttribute("employee");
        dish.setUpdateUser(id);

        // 菜品更改了价格和名称的话，套餐中如果有该菜品，也要跟着一起变动
        Dish dbDish = dishService.getById(dish.getId());
        boolean changeFlag = Objects.equals(dbDish.getName(), dish.getName()) && Objects.equals(dbDish.getPrice(), dish.getPrice());
        if (!changeFlag) {    // 菜品更改了价格和名称的话，套餐中如果有该菜品，也要跟着一起变动
            Long dishId = dish.getId();
            String dishName = dish.getName();
            double dishPrice = dish.getPrice();

            LambdaQueryWrapper<SetmealDish> lqw = new LambdaQueryWrapper<>();
            lqw.eq(SetmealDish::getDishId, dishId);
            List<SetmealDish> list = setmealDishService.list(lqw);

            for (SetmealDish setmealDish: list) {
                setmealDish.setName(dishName);
                setmealDish.setPrice(dishPrice);
                setmealDish.setUpdateUser(id);
                setmealDishService.updateById(setmealDish);
            }
        }

        // 更新
        boolean flag = dishService.updateById(dish);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = dishService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        Dish dish = dishService.getById(id);
        if (dish == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", dish);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/nosort/{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          Dish dish) {
        IPage<Dish> page = dishService.getPage(currentPage, pageSize, dish);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }


    // 2、实际业务开发
    @ApiOperation(value = "分页查询-按照sort值排序结果-推荐用此")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPageBySort(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          Dish dish) {
        IPage<Dish> page = dishService.getPageBySort(currentPage, pageSize, dish);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    @ApiOperation(value = "停售菜品")
    @PutMapping("/stop/{id}")
    public Result stop(HttpServletRequest request, @PathVariable int id) {
        // 哪个用户在修改菜品信息
        Object oid = request.getSession().getAttribute("employee");
        if (oid == null) {
            return Result.fail("未登录");
        }

        // 获得菜品状态信息
        Dish dish = dishService.getById(id);
        if (Objects.isNull(dish)) {
            return Result.fail("无该菜品");
        }

        if (dish.getStatus() == 1) {
            return Result.fail("状态已经更改了");
        }

        // 状态设为异常
        dish.setStatus(1);

        // 更新
        Result rs = update(request, dish);

        if (rs.getFlag()) {
            return Result.success("停售成功");
        } else {
            return Result.fail("操作失败");
        }
    }

    @ApiOperation(value = "起售菜品")
    @PutMapping("/enable/{id}")
    public Result enable(HttpServletRequest request, @PathVariable int id) {
        // 哪个用户在修改菜品信息
        Object oid = request.getSession().getAttribute("employee");
        if (oid == null) {
            return Result.fail("未登录");
        }

        // 获得菜品状态信息
        Dish dish = dishService.getById(id);
        if (Objects.isNull(dish)) {
            return Result.fail("无该菜品");
        }

        if (dish.getStatus() == 0) {
            return Result.fail("状态已经更改了");
        }

        // 状态设为正常
        dish.setStatus(0);

        // 更新
        Result rs = update(request, dish);

        if (rs.getFlag()) {
            return Result.success("起售成功");
        } else {
            return Result.fail("操作失败");
        }
    }

    @ApiOperation(value = "根据分类id返回菜品列表")
    @GetMapping("/getbycategory/{id}")
    public Result getbycategory(@PathVariable int id) {
        LambdaQueryWrapper<Dish> qw = new LambdaQueryWrapper<>();
        qw.eq(Dish::getCategoryId, id);
        List<Dish> list = dishService.list(qw);
        if (list.size() > 0) {
            return Result.success("查询成功", list);
        } else {
            return Result.fail("查询失败");
        }
    }
}

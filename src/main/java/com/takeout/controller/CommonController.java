package com.takeout.controller;

import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * @date 2022/5/22-17:57
 */
@Slf4j
@RestController
@RequestMapping("/commons")
@Api(tags = "通用接口")
public class CommonController {
    @Value("${base-path}")
    private String basePath;

    @PostMapping("upload")
    @ApiOperation(value = "上传文件接口")
    public Result upload(MultipartFile file) {

        // 原始文件名
        String originalFileName = file.getOriginalFilename();
        // 获得后缀
        String suffer = originalFileName.substring(originalFileName.lastIndexOf("."));
        // uuid 重新生成文件名
        String fileName = UUID.randomUUID().toString() + suffer;

        // 创建目录对象
        File dir = new File(basePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            // 转存
            file.transferTo(new File(basePath + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.success(fileName);
    }

    @ApiOperation(value = "下载图片文件接口")
    @GetMapping("/download")
    public void download(HttpServletResponse response, String name) {

        try {
            FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));
            ServletOutputStream outputStream = response.getOutputStream();

            // 设置文件类型
            response.setContentType("image/jpeg");

            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, len);
                outputStream.flush();
            }

            outputStream.close();
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

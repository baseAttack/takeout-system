package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.Employee;
import com.takeout.entity.EmployeeInfo;
import com.takeout.entity.EmployeeLogin;
import com.takeout.service.EmployeeService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @date 2022/5/22-10:42
 */
@Slf4j
@RestController
@RequestMapping("/employees")
@Api(tags = "员工管理接口")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    // 1、CRUD开发, 可省略

    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<Employee> list = employeeService.list();

        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            // 封装Employee数据 到 EmployeeInfo
            List<EmployeeInfo> res = new ArrayList<>();
            for (Employee e : list) {
                EmployeeInfo employeeInfo = new EmployeeInfo(
                        e.getId(),
                        e.getName(),
                        e.getUsername(),
                        e.getPassword(),
                        e.getPhone(),
                        e.getSex(),
                        e.getIdNumber(),
                        e.getStatus()
                );
                res.add(employeeInfo);
            }

            return Result.success("查询成功", res);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody EmployeeInfo employeeInfo) {
        // 用户名不准重复
        LambdaQueryWrapper<Employee> qw = new LambdaQueryWrapper<>();
        qw.eq(Employee::getUsername, employeeInfo.getUsername());
        if (employeeService.count(qw) != 0) {
            return Result.fail("用户名已存在");
        }

        Employee employee = new Employee(
                employeeInfo.getId(),
                employeeInfo.getName(),
                employeeInfo.getUsername(),
                employeeInfo.getPassword(),
                employeeInfo.getPhone(),
                employeeInfo.getSex(),
                employeeInfo.getIdNumber(),
                employeeInfo.getStatus(),
                null, null, null, null
        );

//        // 对密码进行md5加密
        employee.setPassword(DigestUtils.md5DigestAsHex(employee.getPassword().getBytes()));

        // 公共字段
        Long id = (Long)request.getSession().getAttribute("employee");
        employee.setCreateUser(id);
        employee.setUpdateUser(id);

        // 保存
        boolean flag = employeeService.save(employee);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody EmployeeInfo employeeInfo) {
        // 用户名不准重复
//        LambdaQueryWrapper<Employee> qw = new LambdaQueryWrapper<>();
//        qw.eq(Employee::getUsername, employeeInfo.getUsername());
//        if (employeeService.count(qw) != 0) {
//            return Result.fail("用户名已存在");
//        }

        Employee employee2 = employeeService.getById(employeeInfo.getId());

        if (employee2 == null) {
            return Result.fail("没有此人");
        }

        Employee employee = new Employee(
                employeeInfo.getId(),
                employeeInfo.getName(),
                employeeInfo.getUsername(),
                employeeInfo.getPassword(),
                employeeInfo.getPhone(),
                employeeInfo.getSex(),
                employeeInfo.getIdNumber(),
                employeeInfo.getStatus(),
                employee2.getCreateTime(),
                employee2.getUpdateTime(),
                employee2.getCreateUser(),
                employee2.getUpdateUser()
        );

        // 公共字段
        Long id = (Long)request.getSession().getAttribute("employee");
        employee.setUpdateUser(id);

        // 更新
        boolean flag = employeeService.updateById(employee);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = employeeService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        Employee employee = employeeService.getById(id);
        if (employee == null) {
            return Result.fail("查询失败");
        } else {
            EmployeeInfo employeeInfo = new EmployeeInfo(
                    employee.getId(),
                    employee.getName(),
                    employee.getUsername(),
                    employee.getPassword(),
                    employee.getPhone(),
                    employee.getSex(),
                    employee.getIdNumber(),
                    employee.getStatus()
            );

            return Result.success("查询成功", employeeInfo);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          EmployeeInfo employeeInfo0) {
        IPage page = employeeService.getPage(currentPage, pageSize, employeeInfo0);

        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            // 封装数据
            List<EmployeeInfo> employeeInfoList = new ArrayList<>();
            List<Employee> employeeList = page.getRecords();
            for (Employee e : employeeList) {
                EmployeeInfo employeeInfo = new EmployeeInfo(
                        e.getId(),
                        e.getName(),
                        e.getUsername(),
                        e.getPassword(),
                        e.getPhone(),
                        e.getSex(),
                        e.getIdNumber(),
                        e.getStatus()
                );
                employeeInfoList.add(employeeInfo);
            }
            page.setRecords(employeeInfoList);
            return Result.success("分页查询成功", page);
        }
    }

    // 2、具体业务开发
    @ApiOperation(value = "后台管理-员工登录")
    @PostMapping("/login")
    public Result login(HttpServletRequest request, @RequestBody EmployeeLogin employeeLogin) {
        // 如果已经登陆，不允许再次登陆
        if (!Objects.isNull(request.getSession().getAttribute("employee"))) {
            return Result.fail("你已经登陆了!登陆新账户之前请退出!");
        }

        // 1 获取当前密码
        String password = employeeLogin.getPassword();
        // 2 对当前密码进行md5加密
        String password2 = DigestUtils.md5DigestAsHex(password.getBytes());
        // 3 由当前用户名查数据库，返回数据库中的用户
        LambdaQueryWrapper<Employee> qw = new LambdaQueryWrapper<>();
        qw.eq(Employee::getUsername, employeeLogin.getUsername());
        Employee employee2 = employeeService.getOne(qw);
        // 4 查不到
        if (Objects.isNull(employee2)) {
            return Result.fail("登录失败-_-");
        }
        // 5 密码比对
        if (!Objects.equals(password2, employee2.getPassword())) {
            // 不一致
            return Result.fail("登录失败!-!");
        }
        // 6 员工账号是否已经禁用 账号状态,0:正常,1:禁用
        if (employee2.getStatus() != 0) {
            return Result.fail("账号已经被禁用了");
        }

        // 7 审核通过，登陆成功，保存id到session
        request.getSession().setAttribute("employee", employee2.getId());

        // 暂时不弄过滤器，先这样子保存id
//        Long id = (Long) request.getSession().getAttribute("employee");
//        System.out.println("st --- " + id);
//        BaseContext.setCurrentId(id);
//        System.out.println("线程+"+Thread.currentThread().getId());

        EmployeeInfo employeeInfo2 = new EmployeeInfo(
                employee2.getId(),
                employee2.getName(),
                employee2.getUsername(),
                employee2.getPassword(),
                employee2.getPhone(),
                employee2.getSex(),
                employee2.getIdNumber(),
                employee2.getStatus()
        );

        return Result.success("登录成功", employeeInfo2);
    }

    @ApiOperation(value = "后台管理-员工退出")
    @PostMapping("/layout")
    public Result layout(HttpServletRequest request) {
        if (Objects.isNull(request.getSession().getAttribute("employee"))) {
            return Result.fail("未登录");
        } else {
            request.getSession().removeAttribute("employee");
            return Result.success("退出成功^_^");
        }

    }

    @ApiOperation(value = "后台管理-员工禁用")
    @PutMapping("{id}")
    public Result disabled(HttpServletRequest request, @PathVariable Long id) {
        // 没有登陆不能禁用员工
        if (Objects.isNull(request.getSession().getAttribute("employee"))) {
            return Result.fail("没有登陆");
        }

        // 1 禁用员工状态信息

        // 1.1 异常1：修改用户无id，返回失败
        Object objId = request.getSession().getAttribute("employee");
        if (Objects.isNull(objId)) {
            return Result.fail("你尚未登陆");
        }
        // 1.2 异常2：用户不存在，返回失败
        // 获得该员工状态信息
        Employee bannedUser = employeeService.getById(id);
        if (Objects.isNull(bannedUser)) {
            return Result.fail("用户不存在");
        }

        // 该员工的状态信息被禁用
        bannedUser.setStatus(1);

        // 2、记录修改用户的id
        // 禁用的员工信息里 更新 修改的用户的id
        Long manId = (Long)objId ;

        EmployeeInfo bannedUserInfo = new EmployeeInfo(
                bannedUser.getId(),
                bannedUser.getName(),
                bannedUser.getUsername(),
                bannedUser.getPassword(),
                bannedUser.getPhone(),
                bannedUser.getSex(),
                bannedUser.getIdNumber(),
                bannedUser.getStatus()
        );

        Result rs = update(request, bannedUserInfo);

        // 3、 返回结果
        if (rs.getFlag()) {
            return Result.success("禁用成功^_^");
        } else {
            return Result.fail("禁用失败-_-");
        }
    }

    @ApiOperation(value = "后台管理-员工启用")
    @PutMapping("/enable/{id}")
    public Result enable(HttpServletRequest request, @PathVariable Long id) {
        // 没有登陆不能
        if (Objects.isNull(request.getSession().getAttribute("employee"))) {
            return Result.fail("没有登陆");
        }

        // 1 启用员工状态信息

        // 1.1 异常1：修改用户无id，返回失败
        Object objId = request.getSession().getAttribute("employee");
        if (Objects.isNull(objId)) {
            return Result.fail("你尚未登陆");
        }
        // 1.2 异常2：用户不存在，返回失败
        // 获得该员工状态信息
        Employee bannedUser = employeeService.getById(id);
        if (Objects.isNull(bannedUser)) {
            return Result.fail("用户不存在");
        }

        // 该员工的状态信息被启用
        bannedUser.setStatus(0);

        // 2、记录修改用户的id
        // 启用的员工信息里 更新 修改的用户的id
        Long manId = (Long)objId ;

        EmployeeInfo bannedUserInfo = new EmployeeInfo(
                bannedUser.getId(),
                bannedUser.getName(),
                bannedUser.getUsername(),
                bannedUser.getPassword(),
                bannedUser.getPhone(),
                bannedUser.getSex(),
                bannedUser.getIdNumber(),
                bannedUser.getStatus()
        );

        Result rs = update(request, bannedUserInfo);

        // 3、 返回结果
        if (rs.getFlag()) {
            return Result.success("启用成功^_^");
        } else {
            return Result.fail("启用失败-_-");
        }
    }

    @ApiOperation(value = "后台管理-修改密码")
    @PutMapping("/changepassword")
    public Result changePassword(HttpServletRequest request, String password) {
        // 1 修改自己的密码

        // 1.1 异常1：用户未登录，返回失败
        Object objId = request.getSession().getAttribute("employee");
        if (Objects.isNull(objId)) {
            return Result.fail("你尚未登陆");
        }
        // 此员工id
        Long manId = (Long)objId ;
        // 获得该员工状态信息
        Employee employee = employeeService.getById(manId);
        // 对密码进行加密，修改
        employee.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));

        EmployeeInfo updatePasswordEmployeeInfo = new EmployeeInfo(
                employee.getId(),
                employee.getName(),
                employee.getUsername(),
                employee.getPassword(),
                employee.getPhone(),
                employee.getSex(),
                employee.getIdNumber(),
                employee.getStatus()
        );

        Result rs = update(request, updatePasswordEmployeeInfo);

        // 3、 返回结果
        if (rs.getFlag()) {
            return Result.success("修改密码成功^_^");
        } else {
            return Result.fail("修改密码失败-_-");
        }
    }
}

package com.takeout.config;

import com.takeout.filter.Filter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @date 2022/5/29-6:44
 */
//@Configuration  // 暂时不用生效，先注掉
public class FilterConfig {
    // 过滤器1
//    @Bean // 暂时不用生效，先注掉
    public FilterRegistrationBean registrationBean() {
        FilterRegistrationBean filterRegistrationBean  = new FilterRegistrationBean(
                new Filter()    // 自定过滤器
        );
        // 对此网址进行过滤
        filterRegistrationBean.addUrlPatterns("/*");
        // 过滤器名字
//        filterRegistrationBean.setName();
        // 执行顺序
//        filterRegistrationBean.setOrder(1);
        return filterRegistrationBean;
    }
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.SetmealDish;

/**
 * @date 2022/5/22-19:43
 */
public interface SetmealDishService extends IService<SetmealDish> {
    IPage<SetmealDish> getPage(int currentPage, int pageSize);
    IPage<SetmealDish> getPage(int currentPage, int pageSize, SetmealDish setmealDish);
}

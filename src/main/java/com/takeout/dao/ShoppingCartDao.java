package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShoppingCartDao extends BaseMapper<ShoppingCart> {
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.Category;

/**
 * @date 2022/5/22-19:19
 */
public interface CategoryService extends IService<Category> {
    boolean remove (long id);
    IPage<Category> getPage(int currentPage, int pageSize);
    IPage<Category> getPage(int currentPage, int pageSize, Category category);
    IPage<Category> getPageSplitByType(int currentPage, int pageSize, Category category);
}

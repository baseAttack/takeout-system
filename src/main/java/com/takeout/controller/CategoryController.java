package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.Category;
import com.takeout.service.CategoryService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @date 2022/5/22-19:26
 */
@Slf4j
@RestController
@RequestMapping("/categorys")
@Api(tags = "分类管理接口")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    // 1、CRUD
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<Category> list = categoryService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody Category category) {
        Long id = (Long) request.getSession().getAttribute("employee");
        category.setCreateUser(id);
        category.setUpdateUser(id);
        boolean flag = categoryService.save(category);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody Category category) {
        // 公共字段
        Long id = (Long)request.getSession().getAttribute("employee");
        category.setUpdateUser(id);
        // 更新
        boolean flag = categoryService.updateById(category);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = categoryService.remove(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败,请检查: 该分类id是否存在?分类下是否存在套餐或者菜品?");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        Category category = categoryService.getById(id);
        if (category == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", category);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          Category category) {
        IPage<Category> page = categoryService.getPage(currentPage, pageSize, category);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    // 2、业务
    @ApiOperation(value = "分类查询：查所有的菜品或者套餐，以type区分查什么，对数据进行排序，排序值小或者越新的优先。" +
            "输入参数: 在type里输入值")
    @GetMapping("/list")
    public Result list(Category category) {
        // 条件构造器
        LambdaQueryWrapper<Category> qw = new LambdaQueryWrapper<>();
        // 添加条件
        qw.eq(category.getType() != null,
                Category::getType,
                category.getType());
        // 排序条件添加: sort小在前，数据的最新在前
        qw.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        List<Category> list = categoryService.list(qw);

        if (Objects.isNull(list)) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "分类查询：进行分页。查所有的菜品或者套餐，以type区分查什么，对数据进行排序，排序值小或者越新的优先")
    @GetMapping("/list/{currentPage}/{pageSize}")
    public Result getPage2(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          Category category) {
        IPage<Category> page = categoryService.getPageSplitByType(currentPage, pageSize, category);

        if (page.getRecords().size() < 1) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }



}

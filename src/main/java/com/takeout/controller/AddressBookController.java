package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.takeout.entity.AddressBook;
import com.takeout.service.AddressBookService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;


@Slf4j
@RestController
@RequestMapping("/addressbooks")
@Api(tags = "地址簿管理接口")
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;
    // 


    // 业务
    @ApiOperation(value = "新增1")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody AddressBook addressBook) {
        // 设置操作用户
        Long id = (Long) request.getSession().getAttribute("user");
        addressBook.setCreateUser(id);
        addressBook.setUpdateUser(id);

        // save
        boolean flag = addressBookService.save(addressBook);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "设置默认地址")
    @PutMapping("/defaultaddress")
    public Result setDefault(HttpServletRequest request, @RequestBody AddressBook addressBook) {
        // 操作用户
        Long id = (Long) request.getSession().getAttribute("user");

        // 1
        LambdaUpdateWrapper<AddressBook> wrapper = new LambdaUpdateWrapper<>();

        wrapper.eq(AddressBook::getUserId, id);

        wrapper.set((AddressBook::getIsDefault), 0);

        addressBookService.update(wrapper);

        // 2
        addressBook.setIsDefault(1);

        // 保存
        boolean flag = addressBookService.updateById(addressBook);
        return flag ? Result.success("设置成功！") : Result.fail("设置失败");

    }


    @ApiOperation(value = "根据id查询地址")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        AddressBook addressBook = addressBookService.getById(id);
        if (addressBook != null) {
            return Result.success("查询成功!");
        } else {
            return Result.fail("查询失败");
        }
    }

    @ApiOperation(value = "查询默认地址")
    @GetMapping("/defaultaddress")
    public Result getDefaultAddress(HttpServletRequest request) {
        Long id = (Long) request.getSession().getAttribute("user");

        // 条件构造器
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, id);
        queryWrapper.eq(AddressBook::getIsDefault, 1);

        AddressBook addressBook = addressBookService.getOne(queryWrapper);

        if (addressBook != null) {
            return Result.success("查询成功!", addressBook);
        } else {
            return Result.fail("查询失败");
        }
    }

    @ApiOperation(value = "查询指定用户的全部地址")
    @GetMapping("/getalladdress")
    public Result getalladdress(HttpServletRequest request, AddressBook addressBook) {
        // 条件构造器
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(null != addressBook.getUserId(),
                AddressBook::getUserId,
                addressBook.getUserId());
        queryWrapper.orderByDesc(AddressBook::getUpdateTime);

        List<AddressBook> addressBookList = addressBookService.list(queryWrapper);

        if (Objects.isNull(addressBookList)) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功!", addressBookList);
        }
    }
}

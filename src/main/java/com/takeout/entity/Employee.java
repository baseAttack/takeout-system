package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @date 2022/5/22-10:34
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("employee")
@ApiModel(value = "员工-", description = "员工实体类")
public class Employee implements Serializable {
    @ApiModelProperty(value = "员工唯一编号")
    private Long id;
    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "性别,1:男,0:女")
    private String sex;
    @ApiModelProperty(value = "身份证号")
    private String idNumber;
    @ApiModelProperty(value = "账号状态,0:正常,1:禁用")
    private Integer status;

    // 公共字段自动填充
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT)    // 手动添加吧
    @ApiModelProperty(value = "创建员工id")
    private Long createUser;

    @TableField(fill = FieldFill.INSERT_UPDATE) // 手动添加吧
    @ApiModelProperty(value = "更新员工id")
    private Long updateUser;

}

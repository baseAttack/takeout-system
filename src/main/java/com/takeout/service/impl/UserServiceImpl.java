package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.UserDao;
import com.takeout.entity.MyUser;
import com.takeout.service.UserService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/24-19:58
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, MyUser> implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public IPage<MyUser> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        userDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<MyUser> getPage(int currentPage, int pageSize, MyUser myUser) {
        LambdaQueryWrapper<MyUser> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(myUser.getName()),
                MyUser::getName,
                myUser.getName());

        IPage page = new Page(currentPage, pageSize);
        userDao.selectPage(page, lqw);
        return page;
    }
}

package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-19:43
 */
@Mapper
public interface SetmealDishDao extends BaseMapper<SetmealDish> {
}

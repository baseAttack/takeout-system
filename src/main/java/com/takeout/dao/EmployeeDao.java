package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.Employee;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-10:43
 */
@Mapper
public interface EmployeeDao extends BaseMapper<Employee> {

}

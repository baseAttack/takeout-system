package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2022/5/22-19:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("category")
public class Category {
    private Long id;
    private Integer type;
    private String name;
    private Integer sort;
    // 公共字段自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT)    // 手动添加吧
    private Long createUser;
    @TableField(fill = FieldFill.INSERT_UPDATE) // 手动添加吧
    private Long updateUser;
}

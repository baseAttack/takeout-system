package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-19:33
 */
@Mapper
public interface SetmealDao extends BaseMapper<Setmeal> {
}

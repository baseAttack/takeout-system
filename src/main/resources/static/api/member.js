function getMemberList (params) {
    return $axios({
      url: `api/employees/${params.currentPage}/${params.pageSize}`,
      method: 'get',
      data: { ...params }
    })
}
function getMember (params) {
  return $axios({
    url: `api/employees/${params.currentPage}/${params.pageSize}/?username=${params.username}`,
    method: 'get',
    data: { ...params }
  })
}


// 修改---启用禁用接口
function enableOrDisableEmployee (params) {
  return $axios({
    url: 'api/employees',
    method: 'put',
    data: { ...params }
  })
}

// 新增---添加员工
function addEmployee (params) {
  return $axios({
    url: 'api/employees',
    method: 'post',
    data: { ...params }
  })
}

// 修改---添加员工
function editEmployee (params) {
  return $axios({
    url: 'api/employees/',
    method: 'put',
    data: { ...params }
  })
}
function editEmployee1 (params) {
  return $axios({
    url: 'api/employees/',
    method: 'put',
    data: { ...params }
  })
}

// 修改页面反查详情接口
function queryEmployeeById (id) {
  return $axios({
    url: `/api/employees/${id}`,
    method: 'get'
  })
}
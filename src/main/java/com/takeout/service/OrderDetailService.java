package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.OrderDetail;

/**
 * @date 2022/5/22-20:07
 */
public interface OrderDetailService extends IService<OrderDetail> {
    IPage<OrderDetail> getPage(int currentPage, int pageSize);
    IPage<OrderDetail> getPage(int currentPage, int pageSize, OrderDetail orderDetail);
}

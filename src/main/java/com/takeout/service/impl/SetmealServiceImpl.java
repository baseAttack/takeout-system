package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.SetmealDao;
import com.takeout.entity.DishFlavor;
import com.takeout.entity.Setmeal;
import com.takeout.service.SetmealService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;


/**
 * @date 2022/5/22-19:35
 */
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealDao, Setmeal> implements SetmealService {
    @Autowired
    private SetmealDao setmealDao;

    @Override
    public IPage<Setmeal> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        setmealDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Setmeal> getPage(int currentPage, int pageSize, Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(!Objects.isNull(setmeal.getCategoryId()),  // 订单号查询
                Setmeal::getCategoryId,
                setmeal.getCategoryId());
        lqw.like(Strings.isNotEmpty(setmeal.getName()),
                Setmeal::getName,
                setmeal.getName());
        lqw.like(!Objects.isNull(setmeal.getStatus()),
                Setmeal::getStatus,
                setmeal.getStatus());
        lqw.like(Strings.isNotEmpty(setmeal.getDescription()),
                Setmeal::getDescription,
                setmeal.getDescription());

        // 时间范围查询


        IPage page = new Page(currentPage, pageSize);
        setmealDao.selectPage(page, lqw);
        return page;
    }
}

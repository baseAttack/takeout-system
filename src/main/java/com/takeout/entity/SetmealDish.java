package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2022/5/22-14:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("setmeal_dish")
public class SetmealDish {
    private Long id;
    private Long setmealId;//套餐id
    private Long dishId;//菜品id
    private String name;//菜品名称
    private double price;//菜品原价
    private Integer copies;//份数
    private Integer sort;//排序
    private Integer isDeleted;
    // 公共字段自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT)    // 手动
    private Long createUser;
    @TableField(fill = FieldFill.INSERT_UPDATE) // 手动
    private Long updateUser;

}

package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.dao.DishFlavorDao;
import com.takeout.entity.Dish;
import com.takeout.entity.DishFlavor;
import com.takeout.entity.Setmeal;
import com.takeout.service.DishFlavorService;
import com.takeout.service.DishService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @date 2022/5/22-15:59
 */
@Slf4j
@RestController
@RequestMapping("/dishflavors")
@Api(tags = "菜品与口味关系管理接口")
public class DishFlavorController {
    @Autowired
    private DishFlavorService dishFlavorService;

    // 1、CRUD开发, 可省略
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<DishFlavor> list = dishFlavorService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody DishFlavor dishFlavor) {
//        LambdaQueryWrapper<DishFlavor> qw = new LambdaQueryWrapper<>();
//        qw.eq(DishFlavor::getName, dishFlavor.getName());
//        if (dishFlavorService.count(qw) != 0) {
//            return Result.fail("口味名已存在");
//        }

        // 公共字段
        Long id = (Long) request.getSession().getAttribute("employee");
        dishFlavor.setCreateUser(id);
        dishFlavor.setUpdateUser(id);
        // 保存
        boolean flag = dishFlavorService.save(dishFlavor);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody DishFlavor dishFlavor) {
//        LambdaQueryWrapper<DishFlavor> qw = new LambdaQueryWrapper<>();
//        qw.eq(DishFlavor::getName, dishFlavor.getName());
//        if (dishFlavorService.count(qw) != 0) {
//            return Result.fail("口味名已存在");
//        }

        // 公共字段
        Long id = (Long) request.getSession().getAttribute("employee");
        dishFlavor.setUpdateUser(id);
        // 更新
        boolean flag = dishFlavorService.updateById(dishFlavor);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = dishFlavorService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        DishFlavor dishFlavor = dishFlavorService.getById(id);
        if (dishFlavor == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", dishFlavor);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          DishFlavor dishFlavor) {
        IPage<DishFlavor> page = dishFlavorService.getPage(currentPage, pageSize, dishFlavor);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }


    // 2、实际业务开发
    // 根据菜品查口味还要分页？那口味也太多了吧。减少菜品的口味！
    @ApiOperation(value = "根据菜品查口味")
    @GetMapping("/getflavors/{id}")
    public Result getFlavors(@PathVariable int id) {
        // 根据菜品的id，查找菜品口味表内的所有口味
        LambdaQueryWrapper<DishFlavor> qw = new LambdaQueryWrapper<>();
        qw.eq(DishFlavor::getDishId, id);
        List<DishFlavor> list = dishFlavorService.list(qw);

        if (list.size() < 1) {
            return Result.fail("没有查询到!");
        } else {
            int len = list.size();
            return Result.success(String.valueOf(len), list);
        }
    }
}

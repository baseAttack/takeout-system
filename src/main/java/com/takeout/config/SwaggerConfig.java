package com.takeout.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @date 2022/5/22-3:23
 * swagger配置
 * version 2 | swagger-ui.html
 * version 3 | swagger-ui/index.html
 */
@Configuration
@EnableOpenApi
@ConditionalOnProperty(
        value = {"springfox.documentation.enabled"},
        havingValue = "true",
        matchIfMissing = true
)
public class SwaggerConfig {
//    @Value("${swagger.enable:true}")
//    private Boolean enable;

    @Bean
    public Docket createRestApi() {
//        OAS_30
//        SWAGGER_2
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(new ApiInfoBuilder()
                        .title("外卖管理系统的接口文档")
                        .description("外卖管理系统的接口文档")
                        .version("1.1")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.takeout.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}

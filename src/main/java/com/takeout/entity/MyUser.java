package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date 2022/5/24-19:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class MyUser {
    private Long id;
    private String name;
    private String password;
    private String phone;
    private String sex;
    private String idNumber;
    private String avatar;
    private Integer status;
}

package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.ShoppingCartDao;
import com.takeout.entity.ShoppingCart;
import com.takeout.service.ShoppingCartService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartDao, ShoppingCart> implements ShoppingCartService {
    @Autowired
    private ShoppingCartDao shoppingCartDao;

    public IPage<ShoppingCart> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        shoppingCartDao.selectPage(page, null);
        return page;
    }


    @Override
    public IPage<ShoppingCart> getPage(int currentPage, int pageSize, ShoppingCart shoppingCart) {
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(Strings.isNotEmpty(shoppingCart.getName()),    // 姓名查询
                ShoppingCart::getName,
                shoppingCart.getName());

        IPage page = new Page(currentPage, pageSize);
        shoppingCartDao.selectPage(page, lqw);
        return page;
    }
}

package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-19:18
 */
@Mapper
public interface CategoryDao extends BaseMapper<Category> {
}

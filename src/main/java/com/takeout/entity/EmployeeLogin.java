package com.takeout.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date 2022/5/24-14:31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "员工登陆", description = "前端用")
public class EmployeeLogin {
    private String username;
    private String password;
}

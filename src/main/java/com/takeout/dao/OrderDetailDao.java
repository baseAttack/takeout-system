package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-20:06
 */
@Mapper
public interface OrderDetailDao extends BaseMapper<OrderDetail> {
}

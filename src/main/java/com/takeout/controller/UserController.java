package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.MyUser;
import com.takeout.service.UserService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @date 2022/5/24-20:02
 */
@Slf4j
@RestController
@RequestMapping("/users")
@Api(tags = "用户管理接口")
public class UserController {
    @Autowired
    private UserService userService;

    // 1、CRUD开发, 可省略
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<MyUser> list = userService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody MyUser myUser) {
        // 1 用户名
        LambdaQueryWrapper<MyUser> qw = new LambdaQueryWrapper<>();
        qw.eq(MyUser::getName, myUser.getName());
        if (userService.count(qw) != 0) {
            return Result.fail("用户名已存在");
        }

//        // 2 手机号 不能相同
//        LambdaQueryWrapper<MyUser> qw2 = new LambdaQueryWrapper<>();
//        qw2.eq(MyUser::getPhone, myUser.getPhone());
//        if (userService.count(qw2) != 0) {
//            return Result.fail("手机号已存在");
//        }

        // 保存
        boolean flag = userService.save(myUser);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody MyUser myUser) {
        // 更新
        boolean flag = userService.updateById(myUser);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = userService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        MyUser myUser = userService.getById(id);
        if (myUser == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", myUser);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          MyUser myUser) {
        IPage<MyUser> page = userService.getPage(currentPage, pageSize, myUser);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public Result login(HttpServletRequest request, @RequestBody MyUser myUser) {
        LambdaQueryWrapper<MyUser> qw = new LambdaQueryWrapper<>();
        qw.eq(MyUser::getName, myUser.getName());
        MyUser user = userService.getOne(qw);

        request.getSession().setAttribute("user", user.getId());

        if (Objects.equals(user.getPassword(), myUser.getPassword())) {
            return Result.success("登录成功", user);
        } else {
            return Result.fail("登录失败");
        }
    }

    @ApiOperation(value = "退出")
    @PostMapping("/layout")
    public Result login(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        return Result.success("退出成功^_^");
    }




}

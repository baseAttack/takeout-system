package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.DishFlavorDao;
import com.takeout.entity.DishFlavor;
import com.takeout.service.DishFlavorService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-14:54
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorDao, DishFlavor> implements DishFlavorService {
    @Autowired
    private DishFlavorDao dishFlavorDao;


    @Override
    public IPage<DishFlavor> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        dishFlavorDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<DishFlavor> getPage(int currentPage, int pageSize, DishFlavor dishFlavor) {
        LambdaQueryWrapper<DishFlavor> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(null != dishFlavor.getDishId(),
                DishFlavor::getDishId,
                dishFlavor.getDishId());

        IPage page = new Page(currentPage, pageSize);
        dishFlavorDao.selectPage(page, lqw);
        return page;
    }
}

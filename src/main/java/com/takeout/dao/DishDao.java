package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/22-14:51
 */
@Mapper
public interface DishDao extends BaseMapper<Dish> {
}

package com.takeout.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.takeout.entity.MyUser;

/**
 * @date 2022/5/24-19:57
 */
public interface UserService extends IService<MyUser> {
    IPage<MyUser> getPage(int currentPage, int pageSize);
    IPage<MyUser> getPage(int currentPage, int pageSize, MyUser myUser);
}

/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : takeout

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 29/05/2022 05:27:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address_book
-- ----------------------------
DROP TABLE IF EXISTS `address_book`;
CREATE TABLE `address_book`  (
  `id` bigint NOT NULL COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户唯一编号',
  `consignee` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
  `sex` int NOT NULL COMMENT '性别,1:男,0:女',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `province_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份编码',
  `province_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份名称',
  `city_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市编码',
  `city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市名称',
  `district_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县编码	',
  `district_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县名称',
  `detial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址信息',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签（公司、学校、家）	',
  `is_default` int NOT NULL COMMENT '是否是默认收获地址（1:是，0:不是）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint NOT NULL COMMENT '创建员工id',
  `update_user` bigint NOT NULL COMMENT '更新员工id',
  `is_deleted` int NOT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '地址簿表	address_book	' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of address_book
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '分类的id',
  `type` int NULL DEFAULT NULL COMMENT '分类的类型，1:菜品分类 2:套餐分类',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类的名称',
  `sort` int NULL DEFAULT NULL COMMENT '分类的显示顺序',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜品和套餐的分类表	category' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 1, '折扣', 0, '2022-05-22 20:49:24', '2022-05-28 18:49:39', 7, 17);
INSERT INTO `category` VALUES (2, 1, '汉堡', 4, '2022-05-22 20:49:49', '2022-05-22 20:49:52', 7, 7);
INSERT INTO `category` VALUES (3, 1, '小食', 5, '2022-05-22 20:50:39', '2022-05-22 20:50:42', 7, 7);
INSERT INTO `category` VALUES (4, 1, '炸鸡', 9, '2022-05-22 20:51:13', '2022-05-22 20:51:16', 7, 7);
INSERT INTO `category` VALUES (5, 2, '饮品', 8, '2022-05-24 15:57:15', '2022-05-24 15:57:15', 17, 17);
INSERT INTO `category` VALUES (6, 2, '快餐', 7, '2022-05-24 15:57:30', '2022-05-24 15:57:30', 17, 17);
INSERT INTO `category` VALUES (7, 2, '热销', 6, '2022-05-24 16:02:48', '2022-05-24 16:02:48', 17, 17);
INSERT INTO `category` VALUES (8, 2, '优惠', 3, '2022-05-24 16:02:52', '2022-05-24 16:02:52', 17, 17);
INSERT INTO `category` VALUES (9, 2, '主食', 2, '2022-05-24 16:02:53', '2022-05-24 16:02:53', 17, 17);
INSERT INTO `category` VALUES (13, 1, '鸡肉卷', 1, '2022-05-28 18:45:54', '2022-05-28 18:45:54', 17, 17);

-- ----------------------------
-- Table structure for dish
-- ----------------------------
DROP TABLE IF EXISTS `dish`;
CREATE TABLE `dish`  (
  `id` bigint(20) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '菜品唯一编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品名称',
  `category_id` bigint NULL DEFAULT NULL COMMENT '安置菜品的类别的id',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `codevarchar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品码',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片的相对地址',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的描述',
  `status` int NULL DEFAULT NULL COMMENT '状态,0:正常,1:禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  `is_deleted` int NULL DEFAULT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  `sort` int NULL DEFAULT NULL COMMENT '在菜品管理中的顺序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜品表	一种菜' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dish
-- ----------------------------
INSERT INTO `dish` VALUES (00000000000000000001, '西兰花', 1, 1.22, '1kadsuhfdsghkjhgk3566y', 'aea5faad-4343-466c-9c41-3612979972ac.png', '西兰花滋润可口，值得购买', 1, '2022-05-22 15:23:34', '2022-05-29 02:52:30', 7, 17, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000002, '可乐', 1, 0.01, '273gdggfdrtyyu675676yuuytu', 'bbbb.png', '有可乐，满足', 0, '2022-05-22 15:44:25', '2022-05-29 01:40:41', 7, 17, 0, 0);
INSERT INTO `dish` VALUES (00000000000000000003, '苹果', 1, 9.67, 'askfjhdjkfsgh12888181818818', 'aaaaa2.png', '苹果一口一个好过瘾', 1, '2022-05-22 15:46:01', '2022-05-29 01:50:21', 7, 17, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000006, '鸡翅膀', 1, 0.98, '243gdggfdrtyyu675676yuuytu', 'aaaaa3.png', '要可乐请备注', 0, '2022-05-22 07:46:36', '2022-05-23 20:22:43', 7, 17, 0, 3);
INSERT INTO `dish` VALUES (00000000000000000007, '苹果2', 1, 9.80, 'askfjhdjkfsgh12888181818818', '75228ede-d0d5-4074-90bb-a00b11bfb9f5.png', '要可乐请备注!!', 1, '2022-05-22 15:46:23', '2022-05-29 02:53:14', 7, 17, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000008, '鸭脖子', 1, 0.98, '213gdggfdrtyyu675676yuuytu', 'aaaaa5.png', '鸭脖子滋润可口，值得购买', 0, '2022-05-22 16:49:29', '2022-05-23 20:17:02', 7, 17, 0, 3);
INSERT INTO `dish` VALUES (00000000000000000009, '新东西1', 1, 9.90, '214gdggfdrtyyu675676yuuytu', 'aaaaa6.png', '1滋润可口，值得购买', 0, '2022-05-23 20:19:01', '2022-05-23 20:19:01', 17, 17, 0, 5);
INSERT INTO `dish` VALUES (00000000000000000010, '新东西2', 3, 9.90, '215dggfdrtyyu675676yuuytu', 'aaaaa7.png', '2滋润可口，值得购买', 0, '2022-05-23 20:19:06', '2022-05-23 20:19:06', 17, 17, 0, 5);
INSERT INTO `dish` VALUES (00000000000000000011, '新东西3', 2, 9.90, '223gdggfdrtyyu675676yuuytu', 'd59f1969-000d-4a77-847b-124a3c9895c9.png', '3滋润可口，值得购买', 1, '2022-05-23 20:19:07', '2022-05-29 02:52:44', 17, 17, 0, 5);
INSERT INTO `dish` VALUES (00000000000000000012, '香蕉', 1, 23.67, '223gdggfdrtyyu675676yuuyt2', '2a0114fb-d4b9-4f38-af1a-3338fe1c1dff.png', '新鲜的香蕉1斤', 1, '2022-05-29 02:54:31', '2022-05-29 02:54:31', 17, 17, NULL, NULL);
INSERT INTO `dish` VALUES (00000000000000000013, '番茄', 13, 2.30, '223gdggfdrtyyu675676yuuyt3', '48f0acbb-b364-4168-a804-4859d5728fa6.png', '番茄！快来购买吧', 1, '2022-05-29 02:55:41', '2022-05-29 02:55:41', 17, 17, NULL, NULL);
INSERT INTO `dish` VALUES (00000000000000000014, '5斤番茄', 1, 5.60, '61729be6-780d-4666-8f69-e51aaaa2e388', '1437fb48-9263-4410-80ce-0d22078f5e79.png', '特价销售！！', 0, '2022-05-29 02:58:42', '2022-05-29 02:59:48', 17, 17, NULL, NULL);

-- ----------------------------
-- Table structure for dish_flavor
-- ----------------------------
DROP TABLE IF EXISTS `dish_flavor`;
CREATE TABLE `dish_flavor`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自动id',
  `dish_id` bigint NULL DEFAULT NULL COMMENT '菜品的id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区分口味的类别名称',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '每种口味类别的各种值的list',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  `is_deleted` int NULL DEFAULT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜品口味关系表	dish_flavor	一种菜有多种口味' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dish_flavor
-- ----------------------------
INSERT INTO `dish_flavor` VALUES (2, 1, '辣度', '[\"不辣\",\"微辣\",\"中辣\",\"重辣\"]', '2022-05-22 16:44:50', '2022-05-22 16:44:52', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (3, 2, '甜度1', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:52:36', '2022-05-22 16:52:36', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (4, 1, '大小', '[\"大\",\"中\",\"小\"]', '2022-05-22 16:54:55', '2022-05-23 23:13:11', 7, 17, 0);
INSERT INTO `dish_flavor` VALUES (5, 2, '甜度3', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:08', '2022-05-22 16:55:08', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (7, 2, '甜5', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (8, 2, '甜度6', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (10, 2, '甜度8', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (11, 1, '大小2', '[\"大\",\"中\",\"小\"]', '2022-05-23 23:10:30', '2022-05-23 23:10:30', 17, 17, 0);
INSERT INTO `dish_flavor` VALUES (12, 1, '大小3', '[\"大\",\"中\",\"小\"]', '2022-05-23 23:10:55', '2022-05-23 23:10:55', 17, 17, 0);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` bigint(20) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '员工唯一编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别,1:男,0:女',
  `id_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `status` int NULL DEFAULT NULL COMMENT '账号状态,0:正常,1:禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (00000000000000000001, '管理', 'AAA', '14e1b600b1fd579f47433b88e8d85291', '18248889494', '1', '339004200101012929', 0, '2022-05-25 03:00:32', '2022-05-29 05:20:53', 17, 17);
INSERT INTO `employee` VALUES (00000000000000000007, 'QQQ2', 'QQQ1', '714d32d45f6cb3bc336a765119cb3c4c', '18893931111', '0', '339005200001012929', 0, '2022-05-22 13:11:40', '2022-05-24 14:41:29', 1, 17);
INSERT INTO `employee` VALUES (00000000000000000010, 'string1', 'string1', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', 'string', 'string', 0, '2022-05-22 13:55:14', '2022-05-22 13:55:14', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000011, 'string2', 'string2', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', 'string', 'string', 0, '2022-05-22 13:56:40', '2022-05-22 13:56:40', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000012, 'string3', 'string3', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', 'string', 'string', 0, '2022-05-22 13:58:47', '2022-05-22 13:58:47', 7, 7);
INSERT INTO `employee` VALUES (00000000000000000013, '1', 'PPP1', 'b3532077adcf648d539bdb3fcc9587f9', NULL, NULL, NULL, 0, '2022-05-23 18:31:27', '2022-05-23 18:31:27', NULL, NULL);
INSERT INTO `employee` VALUES (00000000000000000017, '朱先生', 'Q', 'fcea920f7412b5da7be0cf42b8c93759', '123456789', '1', '339005200101010010', 0, '2022-05-23 18:54:30', '2022-05-24 14:41:36', NULL, 17);
INSERT INTO `employee` VALUES (00000000000000000018, '黄先生', 'QQ', '7e56035a736d269ad670f312496a0846', '123456789', '1', '339005200101010010', 0, '2022-05-24 14:43:03', '2022-05-24 14:43:03', 17, 17);
INSERT INTO `employee` VALUES (00000000000000000019, '黄先生', 'QQppp', '7e56035a736d269ad670f312496a0846', '123456789', '1', '339005200101010010', 0, '2022-05-24 15:10:28', '2022-05-24 15:10:28', 17, 17);
INSERT INTO `employee` VALUES (00000000000000000020, '拉先生', 'W', '96e79218965eb72c92a549dd5a330112', '122345677', '1', '339005200101044944', 0, '2022-05-25 01:05:14', '2022-05-25 01:05:14', NULL, NULL);
INSERT INTO `employee` VALUES (00000000000000000023, '', 'SSS', 'f542e296af9bd593c4e06b5a31a6eab4', '', '', '', 0, '2022-05-25 02:51:05', '2022-05-25 02:51:05', 17, 17);
INSERT INTO `employee` VALUES (00000000000000000024, 'YOUNAME', 'YOU', 'c855ab4cfc5d81c34b9cdbfdfe785755', '18358889999', '0', NULL, 0, '2022-05-29 04:57:00', '2022-05-29 05:00:50', 17, 17);
INSERT INTO `employee` VALUES (00000000000000000025, '1212121', '1212', 'a01610228fe998f515a72dd730294d87', '18239994949', '1', NULL, 0, '2022-05-29 05:02:11', '2022-05-29 05:04:39', 17, 17);

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品或套餐的名称（dish/setmeal）',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片的相对地址',
  `order_id` bigint NULL DEFAULT NULL COMMENT '订单的id',
  `dish_id` bigint NULL DEFAULT NULL COMMENT '菜品的id',
  `setmeal_id` bigint NULL DEFAULT NULL COMMENT '套餐的id\r\n',
  `dish_flavor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的口味',
  `number` int NULL DEFAULT NULL COMMENT '菜品的数量',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单明细表	order_detail' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单唯一编号',
  `status` int NOT NULL COMMENT '订单状态,0:已取消,1:已下单',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `address_book_id` bigint NOT NULL COMMENT '地址id',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `checkout_time` datetime NOT NULL COMMENT '支付完成的时间',
  `pay_method` int NOT NULL COMMENT '支付方式（各种类型）',
  `amount` decimal(10, 2) NOT NULL COMMENT '金额',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注	',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址信息-文字形式',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名-文字形式',
  `consignee` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表	orders' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 'f6985dc7-7397-4756-ad83-27376ad2d2e5', 1, 7, 0, '2022-05-26 01:32:10', '2022-05-26 01:32:10', 1, 707.97, '无备注', '18358889045', '某地址', '1234', '18358889045');
INSERT INTO `orders` VALUES (2, '11d1daaf-19aa-406e-82b2-6a9933f8f40c', 1, 7, 0, '2022-05-26 01:34:53', '2022-05-26 01:34:53', 1, 110.02, '无备注', '18358889045', '某地址', '1234', '18358889045');
INSERT INTO `orders` VALUES (3, '49ffb0e6-05e4-4289-9a40-4ce3b20c5e28', 1, 7, 0, '2022-05-26 01:35:08', '2022-05-26 01:35:08', 1, 0.00, '无备注', '18358889045', '某地址', '1234', '18358889045');
INSERT INTO `orders` VALUES (4, '3ea8f040-993c-4018-9ef3-61310cd04c02', 1, 7, 0, '2022-05-26 02:28:17', '2022-05-26 02:28:17', 1, 247.75, '无备注', '18358889077', '某地址', '1234', '18358889077');
INSERT INTO `orders` VALUES (5, '9644c72e-cc11-435a-a8e6-147b42ae8242', 1, 7, 0, '2022-05-26 09:50:48', '2022-05-26 09:50:48', 1, 219.05, '无备注', '18358889077', '某地址', '1234', '18358889077');
INSERT INTO `orders` VALUES (6, '1be82531-79d8-4d84-9992-78d0990ac13d', 1, 7, 0, '2022-05-26 17:46:53', '2022-05-26 17:46:53', 1, 399.21, '无备注', '18358889077', '某地址', '1234', '18358889077');
INSERT INTO `orders` VALUES (7, '13b11cd1-1a6a-4dcc-9bf0-6b3aa38a34f5', 1, 11, 0, '2022-05-29 05:15:44', '2022-05-29 05:15:44', 1, 94.48, '无备注', '18358889090', '某地址', 'AAAAA', '18358889090');
INSERT INTO `orders` VALUES (8, '34ca9edd-6ae3-498f-a935-4420c5d7aac4', 1, 11, 0, '2022-05-29 05:16:48', '2022-05-29 05:16:48', 1, 7.33, '无备注', '18358889090', '某地址', 'AAAAA', '18358889090');
INSERT INTO `orders` VALUES (9, 'b1e2fc8f-4b35-4b70-b3d5-6d3c27e808d9', 1, 4, 0, '2022-05-29 05:18:56', '2022-05-29 05:18:56', 1, 6.10, '无备注', '18358779000', '某地址', 'Q', '18358779000');
INSERT INTO `orders` VALUES (10, '8d3ca734-95a9-49c7-918d-25ae81f74576', 1, 4, 0, '2022-05-29 05:19:34', '2022-05-29 05:19:34', 1, 10062.40, '无备注', '18358779000', '某地址', 'Q', '18358779000');

-- ----------------------------
-- Table structure for setmeal
-- ----------------------------
DROP TABLE IF EXISTS `setmeal`;
CREATE TABLE `setmeal`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '套餐的id',
  `category_id` bigint NULL DEFAULT NULL COMMENT '安置套餐的类别的id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐的名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `status` int NULL DEFAULT NULL COMMENT '状态,0:停售,1:起售',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐的编码,对外显示用',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐描述',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐的图片的相对地址',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '套餐表	setmeal	一个套餐' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setmeal
-- ----------------------------
INSERT INTO `setmeal` VALUES (1, 5, '[顶配]招牌手撕大排骨1斤+米饭1份', 20.80, 1, '9121230949320423490234039449394', '越吃越香！！', 'cccc.png', '2022-05-24 00:53:13', '2022-05-29 05:10:28', 17, 17);
INSERT INTO `setmeal` VALUES (2, 5, '大将之风|招牌手撕大排骨1斤+米饭1份', 21.80, 1, '1121230949320423490234039449394', '排骨和米饭', 'cccc2.png', '2022-05-24 00:54:04', '2022-05-29 03:42:18', 17, 17);
INSERT INTO `setmeal` VALUES (3, 5, '软糯可口|招牌猪蹄1只+米饭1份', 1.00, 1, '1121230949320423490234039449394', '好吃！', '18290156-f866-446d-b92a-4f83146b574d.png', '2022-05-24 00:54:13', '2022-05-29 03:54:31', 17, 17);
INSERT INTO `setmeal` VALUES (4, 5, '真香警告|招牌手撕土鸡1只+米饭1份', 9999.00, 1, '1121230949320423490234039449394', '越吃越香', 'cccc4.png', '2022-05-24 00:54:21', '2022-05-29 03:42:26', 17, 17);
INSERT INTO `setmeal` VALUES (5, 5, '扒拉扒拉|五香猪脆骨2斤', 12.00, 1, '3121230949320423490234039449394', '无餐具', 'cccc5.png', '2022-05-24 00:54:44', '2022-05-24 00:54:44', 17, 17);
INSERT INTO `setmeal` VALUES (6, 5, '好汉厉害|手撕大排骨2斤', 45.96, 1, '4121230949320423490234039449394', '越吃越香', 'cccc6.png', '2022-05-24 00:55:08', '2022-05-24 00:55:08', 17, 17);
INSERT INTO `setmeal` VALUES (8, 6, '鸡腿堡+鸡翅+可乐', 0.99, 1, '21312345678679567567', '默认是香辣鸡腿堡哦...', 'cccc7.png', '2022-05-24 00:57:32', '2022-05-24 01:00:19', 17, 17);
INSERT INTO `setmeal` VALUES (11, 5, '12', 12.00, 1, '25e9ee82-f6e1-409a-80ad-19ff952af110', '12', 'be89a98f-0e61-4490-bceb-0282d596a9af.jpg', '2022-05-29 04:01:19', '2022-05-29 04:09:40', 17, 17);

-- ----------------------------
-- Table structure for setmeal_dish
-- ----------------------------
DROP TABLE IF EXISTS `setmeal_dish`;
CREATE TABLE `setmeal_dish`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '唯一编号',
  `setmeal_id` bigint NULL DEFAULT NULL COMMENT '套餐id',
  `dish_id` bigint(20) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '菜品id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '菜品的价格',
  `copies` int NULL DEFAULT NULL COMMENT '菜品的数量',
  `sort` int NULL DEFAULT NULL COMMENT '套餐中菜品的排序',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  `is_deleted` int NULL DEFAULT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '套餐菜品关系表	setmeal_dish	一个套餐里包含多个菜品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setmeal_dish
-- ----------------------------

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品或套餐的名称（dish/setmeal）	',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片的相对地址',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id	',
  `dish_id` bigint NULL DEFAULT NULL COMMENT '菜品的id	',
  `setmeal_id` bigint NULL DEFAULT NULL COMMENT '套餐的id	',
  `dish_flavor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的口味	套餐没有口味',
  `number` int NULL DEFAULT NULL COMMENT '菜品的数量',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间	',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车表	shopping_cart' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------
INSERT INTO `shopping_cart` VALUES (3, 'dish', 'a.jpg', 0, 1, 0, '', 1, 0.90, '2022-05-26 00:10:51');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户唯一编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别,1:男,0:女',
  `id_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像相对地址',
  `status` int NULL DEFAULT NULL COMMENT '账号状态,0:正常,1:禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表-C端	user	' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (3, 'big', '123456', '18358889000', '1', '339005200001011919', 'a.jpg', 0);
INSERT INTO `user` VALUES (4, 'Q', '123456', '18358779000', '0', '339005200001011919', '49022336-df50-447d-b6fb-57a77128e4cd.png', 0);
INSERT INTO `user` VALUES (5, 'E', 'EEEE6', '11111111000', '1', '3119005200001011919', '1.jpg', 0);
INSERT INTO `user` VALUES (6, 'R', '123456', '18358889000', '1', '339005200001011919', 'a.jpg', 0);
INSERT INTO `user` VALUES (7, '1234', '1234', '18358889077', '0', '339004200101016869', 'c79ace56-1ccc-4086-8164-546906ef0c52.jpg', 0);
INSERT INTO `user` VALUES (8, '12345', '12345', '', NULL, '', NULL, 0);
INSERT INTO `user` VALUES (9, '1234567', '1234567', '', '', '', '', 0);
INSERT INTO `user` VALUES (10, '12345678', '12345678', '', '', '', '', 0);
INSERT INTO `user` VALUES (11, 'AAAAA', 'AAAAA', '18358889090', '1', '1243546578678', 'eab6067f-75b5-4876-9b6d-0037448c8b4e.png', 0);

SET FOREIGN_KEY_CHECKS = 1;

package com.takeout.utils;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @date 2022/5/22-11:46
 * mybatis plus 公共字段填充
 */
@Slf4j
@Component
public class FillInPublicField implements MetaObjectHandler {
    /**
     * 插入数据时填充
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime", LocalDateTime.now());
//        metaObject.setValue("createUser", BaseContext.getCurrentId()); // 待改
//        System.out.println("insert----------" + BaseContext.getCurrentId());
//        metaObject.setValue("updateUser", BaseContext.getCurrentId());
//        System.out.println("线程+"+Thread.currentThread().getId());

    }

    /**
     * 修改数据时填充
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        long id = Thread.currentThread().getId();
        metaObject.setValue("updateTime", LocalDateTime.now());
//        metaObject.setValue("updateUser", BaseContext.getCurrentId());
    }
}

package com.takeout.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2022/5/22-19:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("orders")
public class Orders {
    private Long id;
    private String number;
    private Integer status;
    private Long userId;
    private Long addressBookId;
    private LocalDateTime orderTime;
    private LocalDateTime checkoutTime;
    private Integer payMethod;
    private double amount;
    private String remark;
    private String phone;
    private String address;
    private String userName;
    private String consignee;
}

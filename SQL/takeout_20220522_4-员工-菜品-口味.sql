/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : takeout

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 22/05/2022 17:01:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address_book
-- ----------------------------
DROP TABLE IF EXISTS `address_book`;
CREATE TABLE `address_book`  (
  `id` bigint NOT NULL COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户唯一编号',
  `consignee` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
  `sex` int NOT NULL COMMENT '性别,1:男,0:女',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `province_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份编码',
  `province_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份名称',
  `city_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市编码',
  `city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市名称',
  `district_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县编码	',
  `district_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县名称',
  `detial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址信息',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签（公司、学校、家）	',
  `is_default` int NOT NULL COMMENT '是否是默认收获地址（1:是，0:不是）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint NOT NULL COMMENT '创建员工id',
  `update_user` bigint NOT NULL COMMENT '更新员工id',
  `is_deleted` int NOT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '地址簿表	address_book	' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of address_book
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint NOT NULL COMMENT '分类的id',
  `type` int NULL DEFAULT NULL COMMENT '分类的类型，1:菜品分类 2:套餐分类',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类的名称',
  `sort` int NULL DEFAULT NULL COMMENT '分类的显示顺序',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜品和套餐的分类表	category' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------

-- ----------------------------
-- Table structure for dish
-- ----------------------------
DROP TABLE IF EXISTS `dish`;
CREATE TABLE `dish`  (
  `id` bigint(20) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '菜品唯一编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品名称',
  `category_id` bigint NULL DEFAULT NULL COMMENT '安置菜品的类别的id',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `codevarchar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品码',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片的相对地址',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的描述',
  `status` int NULL DEFAULT NULL COMMENT '状态,0:正常,1:禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  `is_deleted` int NULL DEFAULT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  `sort` int NULL DEFAULT NULL COMMENT '在菜品管理中的顺序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜品表	一种菜' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dish
-- ----------------------------
INSERT INTO `dish` VALUES (00000000000000000001, '西兰花0', 1, 10.22, '219387fgdhagisd', '/a/c.png', '这是西兰花', 0, '2022-05-22 15:23:34', '2022-05-22 15:23:37', 7, 7, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000002, 'string', 0, 0.00, 'string', 'string', 'string', 0, '2022-05-22 15:44:25', '2022-05-22 15:44:25', 7, 7, 0, 0);
INSERT INTO `dish` VALUES (00000000000000000003, '苹果', 1, 9.23, 'askfjhdjkfsgh12888181818818', 'a/b.jpg', '这是一个苹果', 0, '2022-05-22 15:46:01', '2022-05-22 15:46:01', 7, 7, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000004, '苹果', 1, 9.23, 'askfjhdjkfsgh12888181818818', 'a/b.jpg', '这是一个苹果', 0, '2022-05-22 15:46:21', '2022-05-22 15:46:21', 7, 7, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000006, '码头', 2, 99.90, '912834uieryaity', 'a.jpg', '这是什么水果', 0, '2022-05-22 07:46:36', '2022-05-22 15:48:27', 7, 7, 0, 0);
INSERT INTO `dish` VALUES (00000000000000000007, '苹果', 1, 9.23, 'askfjhdjkfsgh12888181818818', 'a/b.jpg', '这是一个苹果', 0, '2022-05-22 15:46:23', '2022-05-22 15:46:23', 7, 7, 0, 1);
INSERT INTO `dish` VALUES (00000000000000000008, 'string', 0, 0.00, 'string', 'string', 'string', 0, '2022-05-22 16:49:29', '2022-05-22 16:49:29', 7, 7, 0, 0);

-- ----------------------------
-- Table structure for dish_flavor
-- ----------------------------
DROP TABLE IF EXISTS `dish_flavor`;
CREATE TABLE `dish_flavor`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自动id',
  `dish_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区分口味的类别名称',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '每种口味类别的各种值的list',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  `is_deleted` int NULL DEFAULT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜品口味关系表	dish_flavor	一种菜有多种口味' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dish_flavor
-- ----------------------------
INSERT INTO `dish_flavor` VALUES (1, 'string', 'string', 'string', '2022-05-22 16:50:01', '2022-05-22 16:50:01', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (2, '1', '辣度', '[\"不辣\",\"微辣\",\"中辣\",\"重辣\"]', '2022-05-22 16:44:50', '2022-05-22 16:44:52', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (3, '2', '甜度1', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:52:36', '2022-05-22 16:52:36', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (4, '2', '甜2', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:54:55', '2022-05-22 16:54:55', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (5, '2', '甜度3', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:08', '2022-05-22 16:55:08', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (6, '2', '甜度4', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (7, '2', '甜5', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (8, '2', '甜度6', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);
INSERT INTO `dish_flavor` VALUES (10, '2', '甜度8', '[\"不甜\",\"微甜\",\"中甜\",\"重甜\"]', '2022-05-22 16:55:09', '2022-05-22 16:55:09', 7, 7, 0);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` bigint(20) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '员工唯一编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别,1:男,0:女',
  `id_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `status` int NULL DEFAULT NULL COMMENT '账号状态,0:正常,1:禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (00000000000000000001, '管理', 'AAA', '123456', '18248889494', '1', '339004200101012929', 0, '2022-05-22 12:23:16', '2022-05-22 12:23:16', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000002, '管理3', 'BBB', '222222', '199292929', '0', '339999666666666666', 0, '2022-05-22 12:26:27', '2022-05-22 12:26:27', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000003, '333', '33', '333333', '3333', '1', '333333', 0, '2022-05-22 12:27:02', '2022-05-22 12:35:25', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000004, '管理5', 'DDD', '222222', '199292929', '0', '339999666666666666', 0, '2022-05-22 12:27:13', '2022-05-22 12:27:13', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000006, '管理6', 'EEE', '123', '123', '0', '123123', 0, '2022-05-22 12:36:23', '2022-05-22 12:36:23', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000007, 'QQQ2', 'QQQ', '714d32d45f6cb3bc336a765119cb3c4c', '18893931111', '0', '339005200001012929', 0, '2022-05-22 13:11:40', '2022-05-22 13:11:40', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000008, 'WWW', '22WWW', '793b899a4f8fdf8109b9b0e71aa0f28e', '12122', '0', '121212', 0, '2022-05-22 13:34:32', '2022-05-22 13:34:32', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000009, 'WWW', '22WWW', '793b899a4f8fdf8109b9b0e71aa0f28e', '12122', '0', '121212', 0, '2022-05-22 13:43:30', '2022-05-22 13:43:30', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000010, 'string', 'string', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', 'string', 'string', 0, '2022-05-22 13:55:14', '2022-05-22 13:55:14', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000011, 'string', 'string', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', 'string', 'string', 0, '2022-05-22 13:56:40', '2022-05-22 13:56:40', 1, 1);
INSERT INTO `employee` VALUES (00000000000000000012, 'string', 'string', 'b45cffe084dd3d20d928bee85e7b0f21', 'string', 'string', 'string', 0, '2022-05-22 13:58:47', '2022-05-22 13:58:47', 7, 7);

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品或套餐的名称（dish/setmeal）',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片的相对地址',
  `order_id` bigint NULL DEFAULT NULL COMMENT '订单的id',
  `dish_id` bigint NULL DEFAULT NULL COMMENT '菜品的id',
  `setmeal_id` bigint NULL DEFAULT NULL COMMENT '套餐的id\r\n',
  `dish_flavor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的口味',
  `number` int NULL DEFAULT NULL COMMENT '菜品的数量',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单明细表	order_detail' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint NOT NULL COMMENT '主键',
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单唯一编号',
  `status` int NOT NULL COMMENT '订单状态,0:已取消,1:已下单',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `address_book_id` bigint NOT NULL COMMENT '地址id',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `checkout_time` datetime NOT NULL COMMENT '支付完成的时间',
  `pay_method` int NOT NULL COMMENT '支付方式（各种类型）',
  `amount` decimal(10, 2) NOT NULL COMMENT '金额',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注	',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址信息-文字形式',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名-文字形式',
  `consignee` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表	orders' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for setmeal
-- ----------------------------
DROP TABLE IF EXISTS `setmeal`;
CREATE TABLE `setmeal`  (
  `id` bigint NOT NULL COMMENT '套餐的id',
  `category_id` bigint NULL DEFAULT NULL COMMENT '安置套餐的类别的id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐的名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `status` int NULL DEFAULT NULL COMMENT '状态,0:停售,1:起售',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐的编码,对外显示用',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐描述',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐的图片的相对地址',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '套餐表	setmeal	一个套餐' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setmeal
-- ----------------------------

-- ----------------------------
-- Table structure for setmeal_dish
-- ----------------------------
DROP TABLE IF EXISTS `setmeal_dish`;
CREATE TABLE `setmeal_dish`  (
  `id` bigint NOT NULL COMMENT '唯一编号',
  `setmeal_id` bigint NULL DEFAULT NULL COMMENT '套餐id',
  `dish_id` bigint(20) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '菜品id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '菜品的价格',
  `copies` int NULL DEFAULT NULL COMMENT '菜品的数量',
  `sort` int NULL DEFAULT NULL COMMENT '套餐中菜品的排序',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user` bigint NULL DEFAULT NULL COMMENT '创建员工id',
  `update_user` bigint NULL DEFAULT NULL COMMENT '更新员工id',
  `is_deleted` int NULL DEFAULT NULL COMMENT '是否删除,1:已经删除，0:没有删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '套餐菜品关系表	setmeal_dish	一个套餐里包含多个菜品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setmeal_dish
-- ----------------------------

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品或套餐的名称（dish/setmeal）	',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片的相对地址',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id	',
  `dish_id` bigint NULL DEFAULT NULL COMMENT '菜品的id	',
  `setmeal_id` bigint NULL DEFAULT NULL COMMENT '套餐的id	',
  `dish_flavor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜品的口味	套餐没有口味',
  `number` int NULL DEFAULT NULL COMMENT '菜品的数量',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间	',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车表	shopping_cart' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint NOT NULL COMMENT '用户唯一编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别,1:男,0:女',
  `id_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像相对地址',
  `status` int NULL DEFAULT NULL COMMENT '账号状态,0:正常,1:禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表-C端	user	' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;

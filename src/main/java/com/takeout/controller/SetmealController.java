package com.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.takeout.entity.Dish;
import com.takeout.entity.Setmeal;
import com.takeout.service.DishService;
import com.takeout.service.SetmealService;
import com.takeout.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * @date 2022/5/22-20:19
 */
@RestController
@RequestMapping("/setmeals")
@Slf4j
@Api(tags = "套餐管理接口")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    // 1、CRUD开发, 可省略
    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() {
        List<Setmeal> list = setmealService.list();
        if (Objects.isNull(list)) {
            return Result.fail("没有查询到!");
        } else {
            return Result.success("查询成功", list);
        }
    }

    @ApiOperation(value = "添加1个")
    @PostMapping
    public Result save(HttpServletRequest request, @RequestBody Setmeal setmeal) {
//        LambdaQueryWrapper<Setmeal> qw = new LambdaQueryWrapper<>();
//        qw.eq(Setmeal::getName, setmeal.getName());
//        if (setmealService.count(qw) != 0) {
//            return Result.fail("套餐名已存在");
//        }

        // 公共字段
        Long id = (Long) request.getSession().getAttribute("employee");
        setmeal.setCreateUser(id);
        setmeal.setUpdateUser(id);

        setmeal.setCode(UUID.randomUUID().toString());

        // 保存
        boolean flag = setmealService.save(setmeal);
        return flag ? Result.success("添加成功") : Result.fail("添加失败");
    }

    @ApiOperation(value = "更新1个")
    @PutMapping
    public Result update(HttpServletRequest request, @RequestBody Setmeal setmeal) {
//        LambdaQueryWrapper<Setmeal> qw = new LambdaQueryWrapper<>();
//        qw.eq(Setmeal::getName, setmeal.getName());
//        if (setmealService.count(qw) != 0) {
//            return Result.fail("套餐名已存在");
//        }

        // 公共字段
        Long id = (Long) request.getSession().getAttribute("employee");
        setmeal.setUpdateUser(id);
        // 更新
        boolean flag = setmealService.updateById(setmeal);
        return flag ? Result.success("更新成功") : Result.fail("更新失败");
    }

    @ApiOperation(value = "删除1个")
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Long id) {
        boolean flag = setmealService.removeById(id);
        return flag ? Result.success("删除成功") : Result.fail("删除失败");
    }

    @ApiOperation(value = "查询1个")
    @GetMapping("{id}")
    public Result getById(@PathVariable Long id) {
        Setmeal setmeal = setmealService.getById(id);
        if (setmeal == null) {
            return Result.fail("查询失败");
        } else {
            return Result.success("查询成功", setmeal);
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage,
                          @PathVariable int pageSize,
                          Setmeal setmeal) {
        IPage<Setmeal> page = setmealService.getPage(currentPage, pageSize, setmeal);
        if (page == null) {
            return Result.fail("分页查询失败");
        } else {
            return Result.success("分页查询成功", page);
        }
    }

    // 2 实际业务开发
    @ApiOperation(value = "停售套餐")
    @PutMapping("/stop/{id}")
    public Result stop(HttpServletRequest request, @PathVariable int id) {
        // 哪个用户在修改菜品信息
        Object oid = request.getSession().getAttribute("employee");
        if (oid == null) {
            return Result.fail("未登录");
        }

        // 获得套餐信息
        Setmeal setmeal = setmealService.getById(id);
        if (setmeal == null) {
            return Result.fail("无该套餐");
        }

        if (setmeal.getStatus() == 0) {
            return Result.fail("套餐已经停售了");
        }

        // 设置状态为停售
        setmeal.setStatus(0);

        // 更新
        Result rs = update(request, setmeal);

        if (rs.getFlag()) {
            return Result.success("停售成功");
        } else {
            return Result.fail("操作失败");
        }
    }

    @ApiOperation(value = "启售套餐")
    @PutMapping("/enable/{id}")
    public Result enable(HttpServletRequest request, @PathVariable int id) {
        // 哪个用户在修改菜品信息
        Object oid = request.getSession().getAttribute("employee");
        if (oid == null) {
            return Result.fail("未登录");
        }

        // 获得套餐信息
        Setmeal setmeal = setmealService.getById(id);
        if (setmeal == null) {
            return Result.fail("无该套餐");
        }

        if (setmeal.getStatus() == 1) {
            return Result.fail("套餐已经启售了");
        }

        // 设置状态为启售
        setmeal.setStatus(1);

        // 更新
        Result rs = update(request, setmeal);

        if (rs.getFlag()) {
            return Result.success("启售成功");
        } else {
            return Result.fail("操作失败");
        }
    }

    @ApiOperation(value = "根据分类id返回套餐列表")
    @GetMapping("/getbycategory/{id}")
    public Result getbycategory(@PathVariable int id) {
        LambdaQueryWrapper<Setmeal> qw = new LambdaQueryWrapper<>();
        qw.eq(Setmeal::getCategoryId, id);
        List<Setmeal> list = setmealService.list(qw);
        if (list.size() > 0) {
            return Result.success("查询成功", list);
        } else {
            return Result.fail("查询失败");
        }
    }
}

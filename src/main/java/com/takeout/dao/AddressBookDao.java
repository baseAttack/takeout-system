package com.takeout.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.takeout.entity.AddressBook;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2022/5/24-18:52
 */
@Mapper
public interface AddressBookDao extends BaseMapper<AddressBook> {
}

package com.takeout.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.takeout.dao.SetmealDishDao;
import com.takeout.entity.Dish;
import com.takeout.entity.SetmealDish;
import com.takeout.service.SetmealDishService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2022/5/22-19:44
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishDao, SetmealDish> implements SetmealDishService {

    @Autowired
    private SetmealDishDao setmealDishDao;

    @Override
    public IPage<SetmealDish> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        setmealDishDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<SetmealDish> getPage(int currentPage, int pageSize, SetmealDish setmealDish) {
        LambdaQueryWrapper<SetmealDish> lqw = new LambdaQueryWrapper<>();

        // 模糊查询条件
        lqw.like(setmealDish.getSetmealId() != null,
                SetmealDish::getSetmealId,
                setmealDish.getSetmealId());

        IPage page = new Page(currentPage, pageSize);
        setmealDishDao.selectPage(page, lqw);
        return page;
    }
}

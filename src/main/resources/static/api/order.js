// 查询列表页接口
/*const getOrderDetailPage = (params) => {
  return $axios({
    // url: '/order/page',
    url: '/api/orders',
    method: 'get',
    data: { ...params }
  })
}*/
const getOrderDetailPage = (params) => {
  if (params.number == null) {
    params.number = ""
  }
  else {
    params.number = "/?number=" + params.number
  }
  return $axios({
    // url: '/order/page',
    url: `api/orders/${params.page}/${params.pageSize}`+params.number,
    method: 'get',
    data: { ...params }
  })
}
/*function getPage(params){
  return $axios({
    // url: '/order/page',
    url: '/api/orders/${params.currentPage}/${params.currentPage}',
    method: 'get',
    data: { ...params }
  })
}*/

// 查看接口
const queryOrderDetailById = (id) => {
  return $axios({
    url: `/orderDetail/${id}`,
    method: 'get'
  })
}

// 取消，派送，完成接口
const editOrderDetail = (params) => {
  return $axios({
    url: '/api/orders',
    method: 'put',
    data: { ...params }
  })
}

package com.takeout.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @date 2022/5/22-3:42
 */
@RestController
@Api(tags = "一个测试接口")
public class tController {

    @RequestMapping("/t")
    public String t() {
        return "11";
    }
}
